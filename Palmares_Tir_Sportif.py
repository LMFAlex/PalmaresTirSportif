from reportlab.pdfgen.canvas import Canvas as cv
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import cm

from operator import itemgetter

from tkinter.ttk import *
from tkinter import *

import json
import os

def ajouter_participant():
    global participants
    global categoriesList
    global clubList

    def retour_participant():
        titreAjout.destroy()
        nomText.destroy()
        nomEntree.destroy()
        prenomText.destroy()
        prenomEntree.destroy()
        clubText.destroy()
        clubCombo.destroy()
        ajoutClubText.destroy()
        autreClubEntree.destroy()
        attentionText.destroy()
        categorieText.destroy()
        PGbouton.destroy()
        PFbouton.destroy()
        BGbouton.destroy()
        BFbouton.destroy()
        MGbouton.destroy()
        MFbouton.destroy()
        CGbouton.destroy()
        CFbouton.destroy()
        JGbouton.destroy()
        JFbouton.destroy()
        S1bouton.destroy()
        S2bouton.destroy()
        S3bouton.destroy()
        D1bouton.destroy()
        D2bouton.destroy()
        D3bouton.destroy()
        PARAbouton.destroy()
        PBbouton.destroy()
        PMbouton.destroy()
        PM21bouton.destroy()
        PP21bouton.destroy()
        disciplineText.destroy()
        bouton100.destroy()
        bouton102.destroy()
        bouton103.destroy()
        bouton104.destroy()
        bouton250.destroy()
        bouton251.destroy()
        bouton252.destroy()
        bouton253.destroy()
        bouton501.destroy()
        bouton831.destroy()
        bouton996.destroy()
        bouton171.destroy()
        bouton172.destroy()
        bouton174.destroy()
        bouton181.destroy()
        bouton182.destroy()
        bouton183.destroy()
        bouton184.destroy()
        bouton271.destroy()
        bouton273.destroy()
        bouton274.destroy()
        bouton571.destroy()
        bouton574.destroy()
        boutonCreer.destroy()
        boutonRetourParticipant.destroy()

        fenetre_principale()

    def valider_ajout():
        global participants
        global categoriesList
        global clubList

        disciplines = []

        nom = nomEntree.get()
        prenom = prenomEntree.get()

        if (autreClubEntree.get() == ""):
            club = clubCombo.get()
        else:
            club = autreClubEntree.get()

        categorie = categorieVar.get()
        
        if (var100.get() == 1): disciplines.append(100)
        if (var102.get() == 1): disciplines.append(102)
        if (var103.get() == 1): disciplines.append(103)
        if (var104.get() == 1): disciplines.append(104)
        if (var250.get() == 1): disciplines.append(250)
        if (var251.get() == 1): disciplines.append(251)
        if (var252.get() == 1): disciplines.append(252)
        if (var253.get() == 1): disciplines.append(253)
        if (var501.get() == 1): disciplines.append(501)
        if (var831.get() == 1): disciplines.append(831)
        if (var996.get() == 1): disciplines.append(996)
        if (var171.get() == 1): disciplines.append(171)
        if (var172.get() == 1): disciplines.append(172)
        if (var174.get() == 1): disciplines.append(174)
        if (var181.get() == 1): disciplines.append(181)
        if (var182.get() == 1): disciplines.append(182)
        if (var183.get() == 1): disciplines.append(183)
        if (var184.get() == 1): disciplines.append(184)
        if (var271.get() == 1): disciplines.append(271)
        if (var273.get() == 1): disciplines.append(273)
        if (var274.get() == 1): disciplines.append(274)
        if (var571.get() == 1): disciplines.append(571)
        if (var574.get() == 1): disciplines.append(574)

        dictParticipant = {"nom" : nom, "prenom" : prenom, "club" : club, "disciplines" : disciplines, "categorie" : categorie}

        for discipline in disciplines:
            dictParticipant[str(discipline)] = []
            dictParticipant[str(discipline) + "total"] = -1

        participants.append(dictParticipant)

        titreAjout.destroy()
        nomText.destroy()
        nomEntree.destroy()
        prenomText.destroy()
        prenomEntree.destroy()
        clubText.destroy()
        clubCombo.destroy()
        ajoutClubText.destroy()
        autreClubEntree.destroy()
        attentionText.destroy()
        categorieText.destroy()
        PGbouton.destroy()
        PFbouton.destroy()
        BGbouton.destroy()
        BFbouton.destroy()
        MGbouton.destroy()
        MFbouton.destroy()
        CGbouton.destroy()
        CFbouton.destroy()
        JGbouton.destroy()
        JFbouton.destroy()
        S1bouton.destroy()
        S2bouton.destroy()
        S3bouton.destroy()
        D1bouton.destroy()
        D2bouton.destroy()
        D3bouton.destroy()
        PARAbouton.destroy()
        PBbouton.destroy()
        PMbouton.destroy()
        PM21bouton.destroy()
        PP21bouton.destroy()
        disciplineText.destroy()
        bouton100.destroy()
        bouton102.destroy()
        bouton103.destroy()
        bouton104.destroy()
        bouton250.destroy()
        bouton251.destroy()
        bouton252.destroy()
        bouton253.destroy()
        bouton501.destroy()
        bouton831.destroy()
        bouton996.destroy()
        bouton171.destroy()
        bouton172.destroy()
        bouton174.destroy()
        bouton181.destroy()
        bouton182.destroy()
        bouton183.destroy()
        bouton184.destroy()
        bouton271.destroy()
        bouton273.destroy()
        bouton274.destroy()
        bouton571.destroy()
        bouton574.destroy()
        boutonCreer.destroy()
        boutonRetourParticipant.destroy()

        save_json()
        fenetre_principale()

    boutonAjouter.place_forget()
    infoAjouter.place_forget()
    boutonDiscipline.place_forget()
    infoDiscipline.place_forget()
    boutonSupprimer.place_forget()
    boutonSerie.place_forget()
    boutonPDF.place_forget()
    infoPDF.place_forget()
    boutonSauvegarder.place_forget()
    titre.place_forget()

    titreAjout = Label(fenetre, text="Ajouter Participant")
    titreAjout.place(x=325, y=10)

    nomText = Label(fenetre, text="Nom       (Ne pas oublier les majuscules !)")
    nomText.place(x=50, y=50)

    nomVar = StringVar()
    nomEntree = Entry(fenetre, textvariable=nomVar, width=30)
    nomEntree.place(x=50, y=75)

    prenomText = Label(fenetre, text="Prénom")
    prenomText.place(x=50, y=100)

    prenomVar = StringVar()
    prenomEntree = Entry(fenetre, textvariable=prenomVar, width=30)
    prenomEntree.place(x=50, y=125)

    clubText = Label(fenetre, text="Club")
    clubText.place(x=50, y=150)

    clubVar = StringVar()
    clubVar.set(clubList[0])
    clubCombo = Combobox(fenetre, textvariable=clubVar, values=clubList)
    clubCombo.place(x=50, y=175)

    ajoutClubText = Label(fenetre, text="Autre Club   (facultatif)")
    ajoutClubText.place(x=50, y=200)

    autreClubVar = StringVar()
    autreClubEntree = Entry(fenetre, textvariable=autreClubVar, width=30)
    autreClubEntree.place(x=50, y=225)

    attentionText = Label(fenetre, text="(Attention à toujours l'écrire pareil !)")
    attentionText.place(x=50, y = 250)

    categorieText = Label(fenetre, text="Catégorie")
    categorieText.place(x=50, y=300)

    categorieVar = StringVar() 
    PGbouton = Radiobutton(fenetre, text="PG", variable=categorieVar, value="PG")
    PFbouton = Radiobutton(fenetre, text="PF", variable=categorieVar, value="PF")
    BGbouton = Radiobutton(fenetre, text="BG", variable=categorieVar, value="BG")
    BFbouton = Radiobutton(fenetre, text="BF", variable=categorieVar, value="BF")
    MGbouton = Radiobutton(fenetre, text="MG", variable=categorieVar, value="MG")
    MFbouton = Radiobutton(fenetre, text="MF", variable=categorieVar, value="MF")
    CGbouton = Radiobutton(fenetre, text="CG", variable=categorieVar, value="CG")
    CFbouton = Radiobutton(fenetre, text="CF", variable=categorieVar, value="CF")
    JGbouton = Radiobutton(fenetre, text="JG", variable=categorieVar, value="JG")
    JFbouton = Radiobutton(fenetre, text="JF", variable=categorieVar, value="JF")
    PARAbouton = Radiobutton(fenetre, text="Para-Tir", variable=categorieVar, value="PARA")
    S1bouton = Radiobutton(fenetre, text="S1", variable=categorieVar, value="S1")
    S2bouton = Radiobutton(fenetre, text="S2", variable=categorieVar, value="S2")
    S3bouton = Radiobutton(fenetre, text="S3", variable=categorieVar, value="S3")
    D1bouton = Radiobutton(fenetre, text="D1", variable=categorieVar, value="D1")
    D2bouton = Radiobutton(fenetre, text="D2", variable=categorieVar, value="D2")
    D3bouton = Radiobutton(fenetre, text="D3", variable=categorieVar, value="D3")
    PBbouton = Radiobutton(fenetre, text="PtB", variable=categorieVar, value="PB")
    PMbouton = Radiobutton(fenetre, text="PtM", variable=categorieVar, value="PM")
    PM21bouton = Radiobutton(fenetre, text="Pt-21", variable=categorieVar, value="PM21")
    PP21bouton = Radiobutton(fenetre, text="Pt+21", variable=categorieVar, value="PP21")
    PGbouton.place(x=50, y=320)
    PFbouton.place(x=50, y=340)
    BGbouton.place(x=50, y=360)
    BFbouton.place(x=50, y=380)
    MGbouton.place(x=50, y=400)
    MFbouton.place(x=50, y=420)
    CGbouton.place(x=50, y=440)
    CFbouton.place(x=50, y=460)
    JGbouton.place(x=50, y=480)
    JFbouton.place(x=50, y=500)
    PARAbouton.place(x=50, y= 520)
    S1bouton.place(x=150, y=320)
    S2bouton.place(x=150, y=340)
    S3bouton.place(x=150, y=360)
    D1bouton.place(x=150, y=380)
    D2bouton.place(x=150, y=400)
    D3bouton.place(x=150, y=420)
    PBbouton.place(x=150, y=440)
    PMbouton.place(x=150, y=460)
    PM21bouton.place(x=150, y=480)
    PP21bouton.place(x=150, y=500)

    disciplineText = Label(fenetre, text="Disciplines")
    disciplineText.place(x=450, y=50)

    var100 = IntVar()
    var102 = IntVar()
    var103 = IntVar()
    var104 = IntVar()
    var250 = IntVar()
    var251 = IntVar()
    var252 = IntVar()
    var253 = IntVar()
    var501 = IntVar()
    var831 = IntVar()
    var996 = IntVar()
    var171 = IntVar()
    var172 = IntVar()
    var174 = IntVar()
    var181 = IntVar()
    var182 = IntVar()
    var183 = IntVar()
    var184 = IntVar()
    var271 = IntVar()
    var273 = IntVar()
    var274 = IntVar()
    var571 = IntVar()
    var574 = IntVar()
    bouton100 = Checkbutton(fenetre, text="(100) Pistolet 10m", variable=var100)
    bouton102 = Checkbutton(fenetre, text="(102) Pistolet Vitesse 10m", variable=var102)
    bouton103 = Checkbutton(fenetre, text="(103) Pistolet Standard 10m", variable=var103)
    bouton104 = Checkbutton(fenetre, text="(104) Carabine 10m", variable=var104)
    bouton250 = Checkbutton(fenetre, text="(250) Pistolet 25m", variable=var250)
    bouton251 = Checkbutton(fenetre, text="(251) Pistolet Percussion Centrale", variable=var251)
    bouton252 = Checkbutton(fenetre, text="(252) Pistolet Standard 25m", variable=var252)
    bouton253 = Checkbutton(fenetre, text="(253) Pistolet Vitesse Olympique", variable=var253)
    bouton501 = Checkbutton(fenetre, text="(501) Carabine 50m 60 Balles Couchées", variable=var501)
    bouton831 = Checkbutton(fenetre, text="(831) Pistolet Vitesse Réglementaire", variable=var831)
    bouton996 = Checkbutton(fenetre, text="(996) Carabine Rimfire", variable=var996)
    bouton171 = Checkbutton(fenetre, text="(171) Carabine 10m Debout SH1 Hommes", variable=var171)
    bouton172 = Checkbutton(fenetre, text="(172) Carabine 10m Debout SH1 Dames", variable=var172)
    bouton174 = Checkbutton(fenetre, text="(174) Carabine 10m Debout SH2 Mixte", variable=var174)
    bouton181 = Checkbutton(fenetre, text="(181) Pistolet 10m SH1 Hommes", variable=var181)
    bouton182 = Checkbutton(fenetre, text="(182) Pistolet 10m SH1 Dames", variable=var182)
    bouton183 = Checkbutton(fenetre, text="(183) Pistolet Standard 10m SH1 Mixte", variable=var183)
    bouton184 = Checkbutton(fenetre, text="(184) Pistolet Vitesse 10m SH1 Mixte", variable=var184)
    bouton271 = Checkbutton(fenetre, text="(271) Pistolet 25m SH1 Mixte", variable=var271)
    bouton273 = Checkbutton(fenetre, text="(273) Pistolet Standard 25m SH1 Mixte", variable=var273)
    bouton274 = Checkbutton(fenetre, text="(274) Pistolet Percussion Centrale SH1 Mixte", variable=var274)
    bouton571 = Checkbutton(fenetre, text="(571) Carabine 50m 60 Balles Couchées SH1 Mixte", variable=var571)
    bouton574 = Checkbutton(fenetre, text="(574) Carabine 50m 60 Balles Couchées SH2 Mixte", variable=var574)
    bouton100.place(x=450, y=80)
    bouton102.place(x=450, y=100)
    bouton103.place(x=450, y=120)
    bouton104.place(x=450, y=140)
    bouton250.place(x=450, y=160)
    bouton251.place(x=450, y=180)
    bouton252.place(x=450, y=200)
    bouton253.place(x=450, y=220)
    bouton501.place(x=450, y=240)
    bouton831.place(x=450, y=260)
    bouton996.place(x=450, y=280)
    bouton171.place(x=450, y=300)
    bouton172.place(x=450, y=320)
    bouton174.place(x=450, y=340)
    bouton181.place(x=450, y=360)
    bouton182.place(x=450, y=380)
    bouton183.place(x=450, y=400)
    bouton184.place(x=450, y=420)
    bouton271.place(x=450, y=440)
    bouton273.place(x=450, y=460)
    bouton274.place(x=450, y=480)
    bouton571.place(x=450, y=500)
    bouton574.place(x=450, y=520)

    boutonCreer = Button(fenetre, text="Ajouter Participant", height=2, width=20, command=valider_ajout)
    boutonCreer.place(x=400, y=550)

    boutonRetourParticipant = Button(fenetre, text="Retour", height=2, width=20, command=retour_participant)
    boutonRetourParticipant.place(x=600, y=550)

def ajouter_discipline():
    global participants

    def retour_disciplines():
        titreDisciplines.destroy()
        nomText.destroy()
        nomEntree.destroy()
        prenomText.destroy()
        prenomEntree.destroy()
        disciplineText.destroy()
        bouton100.destroy()
        bouton102.destroy()
        bouton103.destroy()
        bouton104.destroy()
        bouton250.destroy()
        bouton251.destroy()
        bouton252.destroy()
        bouton253.destroy()
        bouton501.destroy()
        bouton831.destroy()
        bouton996.destroy()
        bouton171.destroy()
        bouton172.destroy()
        bouton174.destroy()
        bouton181.destroy()
        bouton182.destroy()
        bouton183.destroy()
        bouton184.destroy()
        bouton271.destroy()
        bouton273.destroy()
        bouton274.destroy()
        bouton571.destroy()
        bouton574.destroy()
        boutonAjouterDisciplines.destroy()
        boutonRetourDisciplines.destroy()

        fenetre_principale()

    def valider_disciplines():
        disciplines = []

        nom = nomEntree.get()
        prenom = prenomEntree.get()

        i = 0
        for participant in participants:
            if ((participant["nom"] == nom) and (participant["prenom"] == prenom)):
                if (var100.get() == 1):
                    try:
                        participants[i]["100total"] = participants[i]["100total"]
                    except:
                        disciplines.append(100)
                if (var102.get() == 1):
                    try:
                        participants[i]["102total"] = participants[i]["102total"]
                    except:
                        disciplines.append(102)
                if (var103.get() == 1):
                    try:
                        participants[i]["103total"] = participants[i]["103total"]
                    except:
                        disciplines.append(103)
                if (var104.get() == 1):
                    try:
                        participants[i]["104total"] = participants[i]["104total"]
                    except:
                        disciplines.append(104)
                if (var250.get() == 1):
                    try:
                        participants[i]["250total"] = participants[i]["250total"]
                    except:
                        disciplines.append(250)
                if (var251.get() == 1):
                    try:
                        participants[i]["251total"] = participants[i]["251total"]
                    except:
                        disciplines.append(251)
                if (var252.get() == 1):
                    try:
                        participants[i]["252total"] = participants[i]["252total"]
                    except:
                        disciplines.append(252)
                if (var253.get() == 1):
                    try:
                        participants[i]["253total"] = participants[i]["253total"]
                    except:
                        disciplines.append(253)
                if (var501.get() == 1):
                    try:
                        participants[i]["501total"] = participants[i]["501total"]
                    except:
                        disciplines.append(501)
                if (var831.get() == 1): 
                    try:
                        participants[i]["831total"] = participants[i]["831total"]
                    except:
                        disciplines.append(831)
                if (var996.get() == 1):
                    try:
                        participants[i]["996total"] = participants[i]["996total"]
                    except:
                        disciplines.append(996)
                if (var171.get() == 1):
                    try:
                        participants[i]["171total"] = participants[i]["171total"]
                    except:
                        disciplines.append(171)
                if (var172.get() == 1):
                    try:
                        participants[i]["172total"] = participants[i]["172total"]
                    except:
                        disciplines.append(172)
                if (var174.get() == 1):
                    try:
                        participants[i]["174total"] = participants[i]["174total"]
                    except:
                        disciplines.append(174)
                if (var181.get() == 1):
                    try:
                        participants[i]["181total"] = participants[i]["181total"]
                    except:
                        disciplines.append(181)
                if (var182.get() == 1):
                    try:
                        participants[i]["182total"] = participants[i]["182total"]
                    except:
                        disciplines.append(182)
                if (var183.get() == 1):
                    try:
                        participants[i]["183total"] = participants[i]["183total"]
                    except:
                        disciplines.append(183)
                if (var184.get() == 1):
                    try:
                        participants[i]["184total"] = participants[i]["184total"]
                    except:
                        disciplines.append(184)
                if (var271.get() == 1):
                    try:
                        participants[i]["271total"] = participants[i]["271total"]
                    except:
                        disciplines.append(271)
                if (var273.get() == 1):
                    try:
                        participants[i]["273total"] = participants[i]["273total"]
                    except:
                        disciplines.append(273)
                if (var274.get() == 1):
                    try:
                        participants[i]["274total"] = participants[i]["274total"]
                    except:
                        disciplines.append(274)
                if (var571.get() == 1):
                    try:
                        participants[i]["571total"] = participants[i]["571total"]
                    except:
                        disciplines.append(571)
                if (var574.get() == 1):
                    try:
                        participants[i]["574total"] = participants[i]["574total"]
                    except:
                        disciplines.append(574)

                for discipline in disciplines:
                    participants[i][str(discipline)] = []
                    participants[i]["disciplines"].append(discipline)
                    participants[i][str(discipline) + "total"] = -1

                titreDisciplines.destroy()
                nomText.destroy()
                nomEntree.destroy()
                prenomText.destroy()
                prenomEntree.destroy()
                disciplineText.destroy()
                bouton100.destroy()
                bouton102.destroy()
                bouton103.destroy()
                bouton104.destroy()
                bouton250.destroy()
                bouton251.destroy()
                bouton252.destroy()
                bouton253.destroy()
                bouton501.destroy()
                bouton831.destroy()
                bouton996.destroy()
                bouton171.destroy()
                bouton172.destroy()
                bouton174.destroy()
                bouton181.destroy()
                bouton182.destroy()
                bouton183.destroy()
                bouton184.destroy()
                bouton271.destroy()
                bouton273.destroy()
                bouton274.destroy()
                bouton571.destroy()
                bouton574.destroy()
                boutonAjouterDisciplines.destroy()
                boutonRetourDisciplines.destroy()

                save_json()
                fenetre_principale()
            i += 1

    boutonAjouter.place_forget()
    infoAjouter.place_forget()
    boutonDiscipline.place_forget()
    infoDiscipline.place_forget()
    boutonSupprimer.place_forget()
    boutonSerie.place_forget()
    boutonPDF.place_forget()
    infoPDF.place_forget()
    boutonSauvegarder.place_forget()
    titre.place_forget()

    titreDisciplines = Label(fenetre, text="Ajouter Disciplines")
    titreDisciplines.place(x=325, y=10)

    nomText = Label(fenetre, text="Nom       (Ne pas oublier les majuscules !)")
    nomText.place(x=50, y=50)

    nomVar = StringVar()
    nomEntree = Entry(fenetre, textvariable=nomVar, width=30)
    nomEntree.place(x=50, y=75)

    prenomText = Label(fenetre, text="Prénom")
    prenomText.place(x=50, y=100)

    prenomVar = StringVar()
    prenomEntree = Entry(fenetre, textvariable=prenomVar, width=30)
    prenomEntree.place(x=50, y=125)

    disciplineText = Label(fenetre, text="Disciplines")
    disciplineText.place(x=450, y=50)

    var100 = IntVar()
    var102 = IntVar()
    var103 = IntVar()
    var104 = IntVar()
    var250 = IntVar()
    var251 = IntVar()
    var252 = IntVar()
    var253 = IntVar()
    var501 = IntVar()
    var831 = IntVar()
    var996 = IntVar()
    var171 = IntVar()
    var172 = IntVar()
    var174 = IntVar()
    var181 = IntVar()
    var182 = IntVar()
    var183 = IntVar()
    var184 = IntVar()
    var271 = IntVar()
    var273 = IntVar()
    var274 = IntVar()
    var571 = IntVar()
    var574 = IntVar()
    bouton100 = Checkbutton(fenetre, text="(100) Pistolet 10m", variable=var100)
    bouton102 = Checkbutton(fenetre, text="(102) Pistolet Vitesse 10m", variable=var102)
    bouton103 = Checkbutton(fenetre, text="(103) Pistolet Standard 10m", variable=var103)
    bouton104 = Checkbutton(fenetre, text="(104) Carabine 10m", variable=var104)
    bouton250 = Checkbutton(fenetre, text="(250) Pistolet 25m", variable=var250)
    bouton251 = Checkbutton(fenetre, text="(251) Pistolet Percussion Centrale", variable=var251)
    bouton252 = Checkbutton(fenetre, text="(252) Pistolet Standard 25m", variable=var252)
    bouton253 = Checkbutton(fenetre, text="(253) Pistolet Vitesse Olympique", variable=var253)
    bouton501 = Checkbutton(fenetre, text="(501) Carabine 50m 60 Balles Couchées", variable=var501)
    bouton831 = Checkbutton(fenetre, text="(831) Pistolet Vitesse Réglementaire", variable=var831)
    bouton996 = Checkbutton(fenetre, text="(996) Carabine Rimfire", variable=var996)
    bouton171 = Checkbutton(fenetre, text="(171) Carabine 10m Debout SH1 Hommes", variable=var171)
    bouton172 = Checkbutton(fenetre, text="(172) Carabine 10m Debout SH1 Dames", variable=var172)
    bouton174 = Checkbutton(fenetre, text="(174) Carabine 10m Debout SH2 Mixte", variable=var174)
    bouton181 = Checkbutton(fenetre, text="(181) Pistolet 10m SH1 Hommes", variable=var181)
    bouton182 = Checkbutton(fenetre, text="(182) Pistolet 10m SH1 Dames", variable=var182)
    bouton183 = Checkbutton(fenetre, text="(183) Pistolet Standard 10m SH1 Mixte", variable=var183)
    bouton184 = Checkbutton(fenetre, text="(184) Pistolet Vitesse 10m SH1 Mixte", variable=var184)
    bouton271 = Checkbutton(fenetre, text="(271) Pistolet 25m SH1 Mixte", variable=var271)
    bouton273 = Checkbutton(fenetre, text="(273) Pistolet Standard 25m SH1 Mixte", variable=var273)
    bouton274 = Checkbutton(fenetre, text="(274) Pistolet Percussion Centrale SH1 Mixte", variable=var274)
    bouton571 = Checkbutton(fenetre, text="(571) Carabine 50m 60 Balles Couchées SH1 Mixte", variable=var571)
    bouton574 = Checkbutton(fenetre, text="(574) Carabine 50m 60 Balles Couchées SH2 Mixte", variable=var574)
    bouton100.place(x=450, y=80)
    bouton102.place(x=450, y=100)
    bouton103.place(x=450, y=120)
    bouton104.place(x=450, y=140)
    bouton250.place(x=450, y=160)
    bouton251.place(x=450, y=180)
    bouton252.place(x=450, y=200)
    bouton253.place(x=450, y=220)
    bouton501.place(x=450, y=240)
    bouton831.place(x=450, y=260)
    bouton996.place(x=450, y=280)
    bouton171.place(x=450, y=300)
    bouton172.place(x=450, y=320)
    bouton174.place(x=450, y=340)
    bouton181.place(x=450, y=360)
    bouton182.place(x=450, y=380)
    bouton183.place(x=450, y=400)
    bouton184.place(x=450, y=420)
    bouton271.place(x=450, y=440)
    bouton273.place(x=450, y=460)
    bouton274.place(x=450, y=480)
    bouton571.place(x=450, y=500)
    bouton574.place(x=450, y=520)

    boutonAjouterDisciplines = Button(fenetre, text="Ajouter Disciplines", height=3, width=20, command=valider_disciplines)
    boutonAjouterDisciplines.place(x=400, y=550)

    boutonRetourDisciplines = Button(fenetre, text="Retour", height=3, width=20, command=retour_disciplines)
    boutonRetourDisciplines.place(x=600, y=550)


def retirer_participant():
    global participants

    def retour_retirer():
        titreAjout.destroy()
        nomText.destroy()
        nomEntree.destroy()
        prenomText.destroy()
        prenomEntree.destroy()
        boutonRetirer.destroy()
        boutonRetourRetirer.destroy()

        fenetre_principale()

    def valider_retirer():
        global participants

        nom = nomEntree.get()
        prenom = prenomEntree.get()

        i = 0
        for participant in participants:
            if ((participant["nom"] == nom) and (participant["prenom"] == prenom)):
                participants.pop(i)
                break
            i += 1

        titreAjout.destroy()
        nomText.destroy()
        nomEntree.destroy()
        prenomText.destroy()
        prenomEntree.destroy()
        boutonRetirer.destroy()
        boutonRetourRetirer.destroy()

        save_json()
        fenetre_principale()

    boutonAjouter.place_forget()
    infoAjouter.place_forget()
    boutonDiscipline.place_forget()
    infoDiscipline.place_forget()
    boutonSupprimer.place_forget()
    boutonSerie.place_forget()
    boutonPDF.place_forget()
    infoPDF.place_forget()
    boutonSauvegarder.place_forget()
    titre.place_forget()

    titreAjout = Label(fenetre, text="Retirer Participant")
    titreAjout.place(x=350, y=10)

    nomText = Label(fenetre, text="Nom       (Ne pas oublier les majuscules !)")
    nomText.place(x=300, y=50)

    nomVar = StringVar()
    nomEntree = Entry(fenetre, textvariable=nomVar, width=30)
    nomEntree.place(x=300, y=75)

    prenomText = Label(fenetre, text="Prénom")
    prenomText.place(x=300, y=100)

    prenomVar = StringVar()
    prenomEntree = Entry(fenetre, textvariable=prenomVar, width=30)
    prenomEntree.place(x=300, y=125)

    boutonRetirer = Button(fenetre, text="Retirer Participant", height=3, width=20, command=valider_retirer)
    boutonRetirer.place(x=225, y=175)

    boutonRetourRetirer = Button(fenetre, text="Retour", height=3, width=20, command=retour_retirer)
    boutonRetourRetirer.place(x=425, y=175)


def ajouter_series():
    global participants

    boutonAjouter.place_forget()
    infoAjouter.place_forget()
    boutonDiscipline.place_forget()
    infoDiscipline.place_forget()
    boutonSupprimer.place_forget()
    boutonSerie.place_forget()
    boutonPDF.place_forget()
    infoPDF.place_forget()
    boutonSauvegarder.place_forget()
    titre.place_forget()

    def definir_series():
        def retour_series():
            for participant in participants:
                if ((participant["nom"] == nom) and (participant["prenom"] == prenom)):
                    for discipline in participant["disciplines"]:
                        if (discipline == 100):
                            if (ordre_dict["100"] == 4):
                                entree1004.destroy()
                            elif (ordre_dict["100"] == 6):
                                entree1004.destroy()
                                entree1005.destroy()
                                entree1006.destroy()
                            entree1001.destroy()
                            entree1002.destroy()
                            entree1003.destroy()
                            label100.destroy()
                        elif (discipline == 102):
                            if (ordre_dict["102"] == 8):
                                entree1027.destroy()
                                entree1028.destroy()
                            entree1021.destroy()
                            entree1022.destroy()
                            entree1023.destroy()
                            entree1024.destroy()
                            entree1025.destroy()
                            entree1026.destroy()
                            label102.destroy()
                        elif (discipline == 103):
                            entree1031.destroy()
                            entree1032.destroy()
                            entree1033.destroy()
                            entree1034.destroy()
                            label103.destroy()
                        elif (discipline == 104):
                            if (ordre_dict["104"] == 4):
                                entree1044.destroy()
                            elif (ordre_dict["104"] == 6):
                                entree1044.destroy()
                                entree1045.destroy()
                                entree1046.destroy()
                            entree1041.destroy()
                            entree1042.destroy()
                            entree1043.destroy()
                            label104.destroy()
                            if (ordre_dict["104"] == 4):
                                entreePE1044.destroy()
                            elif (ordre_dict["104"] == 6):
                                entreePE1044.destroy()
                                entreePE1045.destroy()
                                entreePE1046.destroy()
                            entreePE1041.destroy()
                            entreePE1042.destroy()
                            entreePE1043.destroy()
                            labelPE104.destroy()
                            labelPE1042.destroy()
                        elif (discipline == 250):
                            entree2501.destroy()
                            entree2502.destroy()
                            entree2503.destroy()
                            entree2504.destroy()
                            entree2505.destroy()
                            entree2506.destroy()
                            label250.destroy()
                        elif (discipline == 251):
                            entree2511.destroy()
                            entree2512.destroy()
                            entree2513.destroy()
                            entree2514.destroy()
                            entree2515.destroy()
                            entree2516.destroy()
                            label251.destroy()
                        elif (discipline == 252):
                            entree2521.destroy()
                            entree2522.destroy()
                            entree2523.destroy()
                            entree2524.destroy()
                            entree2525.destroy()
                            entree2526.destroy()
                            label252.destroy()
                        elif (discipline == 253):
                            entree2531.destroy()
                            entree2532.destroy()
                            entree2533.destroy()
                            entree2534.destroy()
                            entree2535.destroy()
                            entree2536.destroy()
                            label253.destroy()
                        elif (discipline == 501):
                            entree5011.destroy()
                            entree5012.destroy()
                            entree5013.destroy()
                            entree5014.destroy()
                            entree5015.destroy()
                            entree5016.destroy()
                            label501.destroy()
                            entreePE5011.destroy()
                            entreePE5012.destroy()
                            entreePE5013.destroy()
                            entreePE5014.destroy()
                            entreePE5015.destroy()
                            entreePE5016.destroy()
                            labelPE501.destroy()
                            labelPE5012.destroy()
                        elif (discipline == 831):
                            entree8311.destroy()
                            entree8312.destroy()
                            label831.destroy()
                        elif (discipline == 996):
                            entree9961.destroy()
                            entree9962.destroy()
                            entree9963.destroy()
                            label996.destroy()
                        elif (discipline == 171):
                            entree1711.destroy()
                            entree1712.destroy()
                            entree1713.destroy()
                            entree1714.destroy()
                            entree1715.destroy()
                            entree1716.destroy()
                            label171.destroy()
                            entreePE1711.destroy()
                            entreePE1712.destroy()
                            entreePE1713.destroy()
                            entreePE1714.destroy()
                            entreePE1715.destroy()
                            entreePE1716.destroy()
                            labelPE171.destroy()
                            labelPE1712.destroy()
                        elif (discipline == 172):
                            entree1721.destroy()
                            entree1722.destroy()
                            entree1723.destroy()
                            entree1724.destroy()
                            entree1725.destroy()
                            entree1726.destroy()
                            label172.destroy()
                            entreePE1721.destroy()
                            entreePE1722.destroy()
                            entreePE1723.destroy()
                            entreePE1724.destroy()
                            entreePE1725.destroy()
                            entreePE1726.destroy()
                            labelPE172.destroy()
                            labelPE1722.destroy()
                        elif (discipline == 174):
                            entree1741.destroy()
                            entree1742.destroy()
                            entree1743.destroy()
                            entree1744.destroy()
                            entree1745.destroy()
                            entree1746.destroy()
                            label174.destroy()
                            entreePE1741.destroy()
                            entreePE1742.destroy()
                            entreePE1743.destroy()
                            entreePE1744.destroy()
                            entreePE1745.destroy()
                            entreePE1746.destroy()
                            labelPE174.destroy()
                            labelPE1742.destroy()
                        elif (discipline == 181):
                            entree1811.destroy()
                            entree1812.destroy()
                            entree1813.destroy()
                            entree1814.destroy()
                            entree1815.destroy()
                            entree1816.destroy()
                            label181.destroy()
                        elif (discipline == 182):
                            entree1821.destroy()
                            entree1822.destroy()
                            entree1823.destroy()
                            entree1824.destroy()
                            entree1825.destroy()
                            entree1826.destroy()
                            label182.destroy()
                        elif (discipline == 183):
                            entree1831.destroy()
                            entree1832.destroy()
                            entree1833.destroy()
                            entree1834.destroy()
                            label183.destroy()
                        elif (discipline == 184):
                            entree1841.destroy()
                            entree1842.destroy()
                            entree1843.destroy()
                            entree1844.destroy()
                            entree1845.destroy()
                            entree1846.destroy()
                            entree1847.destroy()
                            entree1848.destroy()
                            label184.destroy()
                        elif (discipline == 271):
                            entree2711.destroy()
                            entree2712.destroy()
                            entree2713.destroy()
                            entree2714.destroy()
                            entree2715.destroy()
                            entree2716.destroy()
                            label271.destroy()
                        elif (discipline == 273):
                            entree2731.destroy()
                            entree2732.destroy()
                            entree2733.destroy()
                            entree2734.destroy()
                            entree2735.destroy()
                            entree2736.destroy()
                            label273.destroy()
                        elif (discipline == 274):
                            entree2741.destroy()
                            entree2742.destroy()
                            entree2743.destroy()
                            entree2744.destroy()
                            entree2745.destroy()
                            entree2746.destroy()
                            label274.destroy()
                        elif (discipline == 571):
                            entree5711.destroy()
                            entree5712.destroy()
                            entree5713.destroy()
                            entree5714.destroy()
                            entree5715.destroy()
                            entree5716.destroy()
                            label571.destroy()
                            entreePE5711.destroy()
                            entreePE5712.destroy()
                            entreePE5713.destroy()
                            entreePE5714.destroy()
                            entreePE5715.destroy()
                            entreePE5716.destroy()
                            labelPE571.destroy()
                            labelPE5712.destroy()
                        elif (discipline == 574):
                            entree5741.destroy()
                            entree5742.destroy()
                            entree5743.destroy()
                            entree5744.destroy()
                            entree5745.destroy()
                            entree5746.destroy()
                            label574.destroy()
                            entreePE5741.destroy()
                            entreePE5742.destroy()
                            entreePE5743.destroy()
                            entreePE5744.destroy()
                            entreePE5745.destroy()
                            entreePE5746.destroy()
                            labelPE574.destroy()
                            labelPE5742.destroy()

                titreSeries.destroy()
                boutonAjouterSerie.destroy()
                boutonRetourSeries.destroy()
                infosLabel.destroy()
                fenetre_principale()

        def envoyer_series():
            j = 0
            for participant in participants:
                if ((participant["nom"] == nom) and (participant["prenom"] == prenom)):
                    dictParticipant = participant

                    participants.pop(j)

                    for discipline in participant["disciplines"]:
                        series = []

                        if (discipline == 100):
                            series.append(var1001.get())
                            series.append(var1002.get())
                            series.append(var1003.get())

                            if (ordre_dict["100"] == 4):
                                series.append(var1004.get())

                                entree1004.destroy()
                            elif (ordre_dict["100"] == 6):
                                series.append(var1004.get())
                                series.append(var1005.get())
                                series.append(var1006.get())

                                entree1004.destroy()
                                entree1005.destroy()
                                entree1006.destroy()
                            
                            entree1001.destroy()
                            entree1002.destroy()
                            entree1003.destroy()
                            label100.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["100"] = series
                                dictParticipant["100total"] = sum(series)

                        elif (discipline == 102):
                            series.append(var1021.get())
                            series.append(var1022.get())
                            series.append(var1023.get())
                            series.append(var1024.get())
                            series.append(var1025.get())
                            series.append(var1026.get())

                            if (ordre_dict["102"] == 8):
                                series.append(var1027.get())
                                series.append(var1028.get())

                                entree1027.destroy()
                                entree1028.destroy()

                            entree1021.destroy()
                            entree1022.destroy()
                            entree1023.destroy()
                            entree1024.destroy()
                            entree1025.destroy()
                            entree1026.destroy()
                            label102.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["102"] = series
                                dictParticipant["102total"] = sum(series)

                        elif (discipline == 103):
                            series.append(var1031.get())
                            series.append(var1032.get())
                            series.append(var1033.get())
                            series.append(var1034.get())

                            entree1031.destroy()
                            entree1032.destroy()
                            entree1033.destroy()
                            entree1034.destroy()
                            label103.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["103"] = series
                                dictParticipant["103total"] = sum(series)

                        elif (discipline == 104):
                            series.append(var1041.get())
                            series.append(var1042.get())
                            series.append(var1043.get())

                            if (ordre_dict["104"] == 4):
                                series.append(var1044.get())

                                entree1044.destroy()
                            elif (ordre_dict["104"] == 6):
                                series.append(var1044.get())
                                series.append(var1045.get())
                                series.append(var1046.get())

                                entree1044.destroy()
                                entree1045.destroy()
                                entree1046.destroy()
                            
                            entree1041.destroy()
                            entree1042.destroy()
                            entree1043.destroy()
                            label104.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["104"] = series
                                dictParticipant["104total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE1041.get())
                            series.append(varPE1042.get())
                            series.append(varPE1043.get())

                            if (ordre_dict["104"] == 4):
                                series.append(varPE1044.get())

                                entreePE1044.destroy()
                            elif (ordre_dict["104"] == 6):
                                series.append(varPE1044.get())
                                series.append(varPE1045.get())
                                series.append(varPE1046.get())

                                entreePE1044.destroy()
                                entreePE1045.destroy()
                                entreePE1046.destroy()

                            entreePE1041.destroy()
                            entreePE1042.destroy()
                            entreePE1043.destroy()
                            labelPE104.destroy()
                            labelPE1042.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["104PE"] = series
                                dictParticipant["104PEtotal"] = sum(series)

                        elif (discipline == 250):
                            series.append(var2501.get())
                            series.append(var2502.get())
                            series.append(var2503.get())
                            series.append(var2504.get())
                            series.append(var2505.get())
                            series.append(var2506.get())

                            entree2501.destroy()
                            entree2502.destroy()
                            entree2503.destroy()
                            entree2504.destroy()
                            entree2505.destroy()
                            entree2506.destroy()
                            label250.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["250"] = series
                                dictParticipant["250total"] = sum(series)
                        elif (discipline == 251):
                            series.append(var2511.get())
                            series.append(var2512.get())
                            series.append(var2513.get())
                            series.append(var2514.get())
                            series.append(var2515.get())
                            series.append(var2516.get())

                            entree2511.destroy()
                            entree2512.destroy()
                            entree2513.destroy()
                            entree2514.destroy()
                            entree2515.destroy()
                            entree2516.destroy()
                            label251.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["251"] = series
                                dictParticipant["251total"] = sum(series)
                        elif (discipline == 252):
                            series.append(var2521.get())
                            series.append(var2522.get())
                            series.append(var2523.get())
                            series.append(var2524.get())
                            series.append(var2525.get())
                            series.append(var2526.get())

                            entree2521.destroy()
                            entree2522.destroy()
                            entree2523.destroy()
                            entree2524.destroy()
                            entree2525.destroy()
                            entree2526.destroy()
                            label252.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["252"] = series
                                dictParticipant["252total"] = sum(series)
                        elif (discipline == 253):
                            series.append(var2531.get())
                            series.append(var2532.get())
                            series.append(var2533.get())
                            series.append(var2534.get())
                            series.append(var2535.get())
                            series.append(var2536.get())

                            entree2531.destroy()
                            entree2532.destroy()
                            entree2533.destroy()
                            entree2534.destroy()
                            entree2535.destroy()
                            entree2536.destroy()
                            label253.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["253"] = series
                                dictParticipant["253total"] = sum(series)
                        elif (discipline == 501):
                            series.append(var5011.get())
                            series.append(var5012.get())
                            series.append(var5013.get())
                            series.append(var5014.get())
                            series.append(var5015.get())
                            series.append(var5016.get())

                            entree5011.destroy()
                            entree5012.destroy()
                            entree5013.destroy()
                            entree5014.destroy()
                            entree5015.destroy()
                            entree5016.destroy()
                            label501.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["501"] = series
                                dictParticipant["501total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE5011.get())
                            series.append(varPE5012.get())
                            series.append(varPE5013.get())
                            series.append(varPE5014.get())
                            series.append(varPE5015.get())
                            series.append(varPE5016.get())

                            entreePE5011.destroy()
                            entreePE5012.destroy()
                            entreePE5013.destroy()
                            entreePE5014.destroy()
                            entreePE5015.destroy()
                            entreePE5016.destroy()
                            labelPE501.destroy()
                            labelPE5012.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["501PE"] = series
                                dictParticipant["501PEtotal"] = sum(series)
                        elif (discipline == 831):
                            series.append(var8311.get())
                            series.append(var8312.get())

                            entree8311.destroy()
                            entree8312.destroy()
                            label831.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["831"] = series
                                dictParticipant["831total"] = sum(series)
                        elif (discipline == 996):
                            series.append(var9961.get())
                            series.append(var9962.get())
                            series.append(var9963.get())

                            entree9961.destroy()
                            entree9962.destroy()
                            entree9963.destroy()
                            label996.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["996"] = series
                                dictParticipant["996total"] = sum(series)
                        elif (discipline == 171):
                            series.append(var1711.get())
                            series.append(var1712.get())
                            series.append(var1713.get())
                            series.append(var1714.get())
                            series.append(var1715.get())
                            series.append(var1716.get())

                            entree1711.destroy()
                            entree1712.destroy()
                            entree1713.destroy()
                            entree1714.destroy()
                            entree1715.destroy()
                            entree1716.destroy()
                            label171.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["171"] = series
                                dictParticipant["171total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE1711.get())
                            series.append(varPE1712.get())
                            series.append(varPE1713.get())
                            series.append(varPE1714.get())
                            series.append(varPE1715.get())
                            series.append(varPE1716.get())

                            entreePE1711.destroy()
                            entreePE1712.destroy()
                            entreePE1713.destroy()
                            entreePE1714.destroy()
                            entreePE1715.destroy()
                            entreePE1716.destroy()
                            labelPE171.destroy()
                            labelPE1712.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["171PE"] = series
                                dictParticipant["171PEtotal"] = sum(series)
                        elif (discipline == 172):
                            series.append(var1721.get())
                            series.append(var1722.get())
                            series.append(var1723.get())
                            series.append(var1724.get())
                            series.append(var1725.get())
                            series.append(var1726.get())

                            entree1721.destroy()
                            entree1722.destroy()
                            entree1723.destroy()
                            entree1724.destroy()
                            entree1725.destroy()
                            entree1726.destroy()
                            label172.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["172"] = series
                                dictParticipant["172total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE1721.get())
                            series.append(varPE1722.get())
                            series.append(varPE1723.get())
                            series.append(varPE1724.get())
                            series.append(varPE1725.get())
                            series.append(varPE1726.get())

                            entreePE1721.destroy()
                            entreePE1722.destroy()
                            entreePE1723.destroy()
                            entreePE1724.destroy()
                            entreePE1725.destroy()
                            entreePE1726.destroy()
                            labelPE172.destroy()
                            labelPE1722.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["172PE"] = series
                                dictParticipant["172PEtotal"] = sum(series)
                        elif (discipline == 174):
                            series.append(var1741.get())
                            series.append(var1742.get())
                            series.append(var1743.get())
                            series.append(var1744.get())
                            series.append(var1745.get())
                            series.append(var1746.get())

                            entree1741.destroy()
                            entree1742.destroy()
                            entree1743.destroy()
                            entree1744.destroy()
                            entree1745.destroy()
                            entree1746.destroy()
                            label174.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["174"] = series
                                dictParticipant["174total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE1741.get())
                            series.append(varPE1742.get())
                            series.append(varPE1743.get())
                            series.append(varPE1744.get())
                            series.append(varPE1745.get())
                            series.append(varPE1746.get())

                            entreePE1741.destroy()
                            entreePE1742.destroy()
                            entreePE1743.destroy()
                            entreePE1744.destroy()
                            entreePE1745.destroy()
                            entreePE1746.destroy()
                            labelPE174.destroy()
                            labelPE1742.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["174PE"] = series
                                dictParticipant["174PEtotal"] = sum(series)
                        elif (discipline == 181):
                            series.append(var1811.get())
                            series.append(var1812.get())
                            series.append(var1813.get())
                            series.append(var1814.get())
                            series.append(var1815.get())
                            series.append(var1816.get())

                            entree1811.destroy()
                            entree1812.destroy()
                            entree1813.destroy()
                            entree1814.destroy()
                            entree1815.destroy()
                            entree1816.destroy()
                            label181.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["181"] = series
                                dictParticipant["181total"] = round(sum(series), 1)
                        elif (discipline == 182):
                            series.append(var1821.get())
                            series.append(var1822.get())
                            series.append(var1823.get())
                            series.append(var1824.get())
                            series.append(var1825.get())
                            series.append(var1826.get())

                            entree1821.destroy()
                            entree1822.destroy()
                            entree1823.destroy()
                            entree1824.destroy()
                            entree1825.destroy()
                            entree1826.destroy()
                            label182.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["182"] = series
                                dictParticipant["182total"] = round(sum(series), 1)
                        elif (discipline == 183):
                            series.append(var1831.get())
                            series.append(var1832.get())
                            series.append(var1833.get())
                            series.append(var1834.get())

                            entree1831.destroy()
                            entree1832.destroy()
                            entree1833.destroy()
                            entree1834.destroy()
                            label183.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["183"] = series
                                dictParticipant["183total"] = round(sum(series), 1)
                        elif (discipline == 184):
                            series.append(var1841.get())
                            series.append(var1842.get())
                            series.append(var1843.get())
                            series.append(var1844.get())
                            series.append(var1845.get())
                            series.append(var1846.get())
                            series.append(var1847.get())
                            series.append(var1848.get())

                            entree1841.destroy()
                            entree1842.destroy()
                            entree1843.destroy()
                            entree1844.destroy()
                            entree1845.destroy()
                            entree1846.destroy()
                            entree1847.destroy()
                            entree1848.destroy()
                            label184.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["184"] = series
                                dictParticipant["184total"] = round(sum(series), 1)
                        elif (discipline == 271):
                            series.append(var2711.get())
                            series.append(var2712.get())
                            series.append(var2713.get())
                            series.append(var2714.get())
                            series.append(var2715.get())
                            series.append(var2716.get())

                            entree2711.destroy()
                            entree2712.destroy()
                            entree2713.destroy()
                            entree2714.destroy()
                            entree2715.destroy()
                            entree2716.destroy()
                            label271.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["271"] = series
                                dictParticipant["271total"] = round(sum(series), 1)
                        elif (discipline == 273):
                            series.append(var2731.get())
                            series.append(var2732.get())
                            series.append(var2733.get())
                            series.append(var2734.get())
                            series.append(var2735.get())
                            series.append(var2736.get())

                            entree2731.destroy()
                            entree2732.destroy()
                            entree2733.destroy()
                            entree2734.destroy()
                            entree2735.destroy()
                            entree2736.destroy()
                            label273.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["273"] = series
                                dictParticipant["273total"] = round(sum(series), 1)
                        elif (discipline == 274):
                            series.append(var2741.get())
                            series.append(var2742.get())
                            series.append(var2743.get())
                            series.append(var2744.get())
                            series.append(var2745.get())
                            series.append(var2746.get())

                            entree2741.destroy()
                            entree2742.destroy()
                            entree2743.destroy()
                            entree2744.destroy()
                            entree2745.destroy()
                            entree2746.destroy()
                            label274.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["274"] = series
                                dictParticipant["274total"] = round(sum(series), 1)
                        elif (discipline == 571):
                            series.append(var5711.get())
                            series.append(var5712.get())
                            series.append(var5713.get())
                            series.append(var5714.get())
                            series.append(var5715.get())
                            series.append(var5716.get())

                            entree5711.destroy()
                            entree5712.destroy()
                            entree5713.destroy()
                            entree5714.destroy()
                            entree5715.destroy()
                            entree5716.destroy()
                            label571.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["571"] = series
                                dictParticipant["571total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE5711.get())
                            series.append(varPE5712.get())
                            series.append(varPE5713.get())
                            series.append(varPE5714.get())
                            series.append(varPE5715.get())
                            series.append(varPE5716.get())

                            entreePE5711.destroy()
                            entreePE5712.destroy()
                            entreePE5713.destroy()
                            entreePE5714.destroy()
                            entreePE5715.destroy()
                            entreePE5716.destroy()
                            labelPE571.destroy()
                            labelPE5712.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["571PE"] = series
                                dictParticipant["571PEtotal"] = sum(series)
                        elif (discipline == 574):
                            series.append(var5741.get())
                            series.append(var5742.get())
                            series.append(var5743.get())
                            series.append(var5744.get())
                            series.append(var5745.get())
                            series.append(var5746.get())

                            entree5741.destroy()
                            entree5742.destroy()
                            entree5743.destroy()
                            entree5744.destroy()
                            entree5745.destroy()
                            entree5746.destroy()
                            label574.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["574"] = series
                                dictParticipant["574total"] = round(sum(series), 1)

                            series = []

                            series.append(varPE5741.get())
                            series.append(varPE5742.get())
                            series.append(varPE5743.get())
                            series.append(varPE5744.get())
                            series.append(varPE5745.get())
                            series.append(varPE5746.get())

                            entreePE5741.destroy()
                            entreePE5742.destroy()
                            entreePE5743.destroy()
                            entreePE5744.destroy()
                            entreePE5745.destroy()
                            entreePE5746.destroy()
                            labelPE574.destroy()
                            labelPE5742.destroy()

                            if (sum(series) != 0) :
                                dictParticipant["574PE"] = series
                                dictParticipant["574PEtotal"] = sum(series)                      

                    participants.append(dictParticipant)

                    titreSeries.destroy()
                    boutonAjouterSerie.destroy()
                    boutonRetourSeries.destroy()
                    infosLabel.destroy()
                    save_json()
                    fenetre_principale()
                j += 1

        hauteur = 50
        largeur = 50

        nom = nomEntree.get()
        prenom = prenomEntree.get()

        i = 0
        for participant in participants:
            if ((participant["nom"] == nom) and (participant["prenom"] == prenom)):
                demanderParticipant.destroy()
                nomText.destroy()
                nomEntree.destroy()
                prenomText.destroy()
                prenomEntree.destroy()
                boutonValider.destroy()
                boutonRetour.destroy()

                ordre_dict = {}
                
                for discipline in participant["disciplines"]:

                    if (discipline == 100):

                        disciplineStr = "Pistolet 10m (100)"

                        trente = ["PJ", "PG", "PF", "BG", "BF"]
                        quarante = ["MG", "MF", "D3"]

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label100 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label100.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        if (participant["categorie"] in trente): 
                            nbSeries = 3

                            var1001 = IntVar()
                            entree1001 = Entry(fenetre, textvariable=var1001, width=3)
                            entree1001.place(x=largeur, y=hauteur)

                            var1002 = IntVar()
                            entree1002 = Entry(fenetre, textvariable=var1002, width=3)
                            entree1002.place(x=largeur + 30, y=hauteur)

                            var1003 = IntVar()
                            entree1003 = Entry(fenetre, textvariable=var1003, width=3)
                            entree1003.place(x=largeur + 60, y=hauteur)
                        elif(participant["categorie"] in quarante):
                            nbSeries = 4

                            var1001 = IntVar()
                            entree1001 = Entry(fenetre, textvariable=var1001, width=3)
                            entree1001.place(x=largeur, y=hauteur)

                            var1002 = IntVar()
                            entree1002 = Entry(fenetre, textvariable=var1002, width=3)
                            entree1002.place(x=largeur + 30, y=hauteur)

                            var1003 = IntVar()
                            entree1003 = Entry(fenetre, textvariable=var1003, width=3)
                            entree1003.place(x=largeur + 60, y=hauteur)

                            var1004 = IntVar()
                            entree1004 = Entry(fenetre, textvariable=var1004, width=3)
                            entree1004.place(x=largeur + 90, y=hauteur)
                        else:
                            nbSeries = 6

                            var1001 = IntVar()
                            entree1001 = Entry(fenetre, textvariable=var1001, width=3)
                            entree1001.place(x=largeur, y=hauteur)

                            var1002 = IntVar()
                            entree1002 = Entry(fenetre, textvariable=var1002, width=3)
                            entree1002.place(x=largeur + 30, y=hauteur)

                            var1003 = IntVar()
                            entree1003 = Entry(fenetre, textvariable=var1003, width=3)
                            entree1003.place(x=largeur + 60, y=hauteur)

                            var1004 = IntVar()
                            entree1004 = Entry(fenetre, textvariable=var1004, width=3)
                            entree1004.place(x=largeur + 90, y=hauteur)

                            var1005 = IntVar()
                            entree1005 = Entry(fenetre, textvariable=var1005, width=3)
                            entree1005.place(x=largeur + 120, y=hauteur)

                            var1006 = IntVar()
                            entree1006 = Entry(fenetre, textvariable=var1006, width=3)
                            entree1006.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["100"] = nbSeries

                    elif (discipline == 102):

                        disciplineStr = "Pistolet Vitesse 10m (102)"

                        trente = ["MG", "MF"]
                        
                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label102 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label102.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                        
                        if (participant["categorie"] in trente): 
                            nbSeries = 6

                            var1021 = IntVar()
                            entree1021 = Entry(fenetre, textvariable=var1021, width=3)
                            entree1021.place(x=largeur, y=hauteur)

                            var1022 = IntVar()
                            entree1022 = Entry(fenetre, textvariable=var1022, width=3)
                            entree1022.place(x=largeur + 30, y=hauteur)

                            var1023 = IntVar()
                            entree1023 = Entry(fenetre, textvariable=var1023, width=3)
                            entree1023.place(x=largeur + 60, y=hauteur)

                            var1024 = IntVar()
                            entree1024 = Entry(fenetre, textvariable=var1024, width=3)
                            entree1024.place(x=largeur + 90, y=hauteur)

                            var1025 = IntVar()
                            entree1025 = Entry(fenetre, textvariable=var1025, width=3)
                            entree1025.place(x=largeur + 120, y=hauteur)

                            var1026 = IntVar()
                            entree1026 = Entry(fenetre, textvariable=var1026, width=3)
                            entree1026.place(x=largeur + 150, y=hauteur)
                        else:
                            nbSeries = 8

                            var1021 = IntVar()
                            entree1021 = Entry(fenetre, textvariable=var1021, width=3)
                            entree1021.place(x=largeur, y=hauteur)

                            var1022 = IntVar()
                            entree1022 = Entry(fenetre, textvariable=var1022, width=3)
                            entree1022.place(x=largeur + 30, y=hauteur)

                            var1023 = IntVar()
                            entree1023 = Entry(fenetre, textvariable=var1023, width=3)
                            entree1023.place(x=largeur + 60, y=hauteur)

                            var1024 = IntVar()
                            entree1024 = Entry(fenetre, textvariable=var1024, width=3)
                            entree1024.place(x=largeur + 90, y=hauteur)

                            var1025 = IntVar()
                            entree1025 = Entry(fenetre, textvariable=var1025, width=3)
                            entree1025.place(x=largeur + 120, y=hauteur)

                            var1026 = IntVar()
                            entree1026 = Entry(fenetre, textvariable=var1026, width=3)
                            entree1026.place(x=largeur + 150, y=hauteur)

                            var1027 = IntVar()
                            entree1027 = Entry(fenetre, textvariable=var1027, width=3)
                            entree1027.place(x=largeur + 180, y=hauteur)

                            var1028 = IntVar()
                            entree1028 = Entry(fenetre, textvariable=var1028, width=3)
                            entree1028.place(x=largeur + 210, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["102"] = nbSeries

                    elif (discipline == 103):

                        disciplineStr = "Pistolet Standard 10m (103)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350
                    
                        label103 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label103.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                        
                        nbSeries = 4

                        var1031 = IntVar()
                        entree1031 = Entry(fenetre, textvariable=var1031, width=3)
                        entree1031.place(x=largeur, y=hauteur)

                        var1032 = IntVar()
                        entree1032 = Entry(fenetre, textvariable=var1032, width=3)
                        entree1032.place(x=largeur + 30, y=hauteur)

                        var1033 = IntVar()
                        entree1033 = Entry(fenetre, textvariable=var1033, width=3)
                        entree1033.place(x=largeur + 60, y=hauteur)

                        var1034 = IntVar()
                        entree1034 = Entry(fenetre, textvariable=var1034, width=3)
                        entree1034.place(x=largeur + 90, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["103"] = nbSeries

                    elif (discipline == 104):

                        disciplineStr = "Carabine 10m (104)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label104 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label104.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                        
                        trente = ["PJ", "PG", "PF", "BG", "BF", "PB"]
                        quarante = ["MG", "MF", "D3", "PM"]

                        if (participant["categorie"] in trente):
                            nbSeries = 3

                            var1041 = DoubleVar()
                            entree1041 = Entry(fenetre, textvariable=var1041, width=4)
                            entree1041.place(x=largeur, y=hauteur)

                            var1042 = DoubleVar()
                            entree1042 = Entry(fenetre, textvariable=var1042, width=4)
                            entree1042.place(x=largeur + 30, y=hauteur)

                            var1043 = DoubleVar()
                            entree1043 = Entry(fenetre, textvariable=var1043, width=4)
                            entree1043.place(x=largeur + 60, y=hauteur)
                        elif(participant["categorie"] in quarante):
                            nbSeries = 4

                            var1041 = DoubleVar()
                            entree1041 = Entry(fenetre, textvariable=var1041, width=4)
                            entree1041.place(x=largeur, y=hauteur)

                            var1042 = DoubleVar()
                            entree1042 = Entry(fenetre, textvariable=var1042, width=4)
                            entree1042.place(x=largeur + 30, y=hauteur)

                            var1043 = DoubleVar()
                            entree1043 = Entry(fenetre, textvariable=var1043, width=4)
                            entree1043.place(x=largeur + 60, y=hauteur)

                            var1044 = DoubleVar()
                            entree1044 = Entry(fenetre, textvariable=var1044, width=4)
                            entree1044.place(x=largeur + 90, y=hauteur)
                        else:
                            nbSeries = 6

                            var1041 = DoubleVar()
                            entree1041 = Entry(fenetre, textvariable=var1041, width=4)
                            entree1041.place(x=largeur, y=hauteur)

                            var1042 = DoubleVar()
                            entree1042 = Entry(fenetre, textvariable=var1042, width=4)
                            entree1042.place(x=largeur + 30, y=hauteur)

                            var1043 = DoubleVar()
                            entree1043 = Entry(fenetre, textvariable=var1043, width=4)
                            entree1043.place(x=largeur + 60, y=hauteur)

                            var1044 = DoubleVar()
                            entree1044 = Entry(fenetre, textvariable=var1044, width=4)
                            entree1044.place(x=largeur + 90, y=hauteur)

                            var1045 = DoubleVar()
                            entree1045 = Entry(fenetre, textvariable=var1045, width=4)
                            entree1045.place(x=largeur + 120, y=hauteur)

                            var1046 = DoubleVar()
                            entree1046 = Entry(fenetre, textvariable=var1046, width=4)
                            entree1046.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["104"] = nbSeries

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE104 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE104.place(x=largeur, y=hauteur)
                        labelPE1042 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE1042.place(x=largeur, y=hauteur + 25)

                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        if (participant["categorie"] in trente):
                            nbSeries = 3

                            varPE1041 = IntVar()
                            entreePE1041 = Entry(fenetre, textvariable=varPE1041, width=3)
                            entreePE1041.place(x=largeur, y=hauteur)

                            varPE1042 = IntVar()
                            entreePE1042 = Entry(fenetre, textvariable=varPE1042, width=3)
                            entreePE1042.place(x=largeur + 30, y=hauteur)

                            varPE1043 = IntVar()
                            entreePE1043 = Entry(fenetre, textvariable=varPE1043, width=3)
                            entreePE1043.place(x=largeur + 60, y=hauteur)
                        elif(participant["categorie"] in quarante):
                            nbSeries = 4

                            varPE1041 = IntVar()
                            entreePE1041 = Entry(fenetre, textvariable=varPE1041, width=3)
                            entreePE1041.place(x=largeur, y=hauteur)

                            varPE1042 = IntVar()
                            entreePE1042 = Entry(fenetre, textvariable=varPE1042, width=3)
                            entreePE1042.place(x=largeur + 30, y=hauteur)

                            varPE1043 = IntVar()
                            entreePE1043 = Entry(fenetre, textvariable=varPE1043, width=3)
                            entreePE1043.place(x=largeur + 60, y=hauteur)

                            varPE1044 = IntVar()
                            entreePE1044 = Entry(fenetre, textvariable=varPE1044, width=3)
                            entreePE1044.place(x=largeur + 90, y=hauteur)
                        else:
                            nbSeries = 6

                            varPE1041 = IntVar()
                            entreePE1041 = Entry(fenetre, textvariable=varPE1041, width=3)
                            entreePE1041.place(x=largeur, y=hauteur)

                            varPE1042 = IntVar()
                            entreePE1042 = Entry(fenetre, textvariable=varPE1042, width=3)
                            entreePE1042.place(x=largeur + 30, y=hauteur)

                            varPE1043 = IntVar()
                            entreePE1043 = Entry(fenetre, textvariable=varPE1043, width=3)
                            entreePE1043.place(x=largeur + 60, y=hauteur)

                            varPE1044 = IntVar()
                            entreePE1044 = Entry(fenetre, textvariable=varPE1044, width=3)
                            entreePE1044.place(x=largeur + 90, y=hauteur)

                            varPE1045 = IntVar()
                            entreePE1045 = Entry(fenetre, textvariable=varPE1045, width=3)
                            entreePE1045.place(x=largeur + 120, y=hauteur)

                            varPE1046 = IntVar()
                            entreePE1046 = Entry(fenetre, textvariable=varPE1046, width=3)
                            entreePE1046.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 250):

                        disciplineStr = "Pistolet 25m (250)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label250 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label250.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6
                        
                        var2501 = IntVar()
                        entree2501 = Entry(fenetre, textvariable=var2501, width=3)
                        entree2501.place(x=largeur, y=hauteur)

                        var2502 = IntVar()
                        entree2502 = Entry(fenetre, textvariable=var2502, width=3)
                        entree2502.place(x=largeur + 30, y=hauteur)

                        var2503 = IntVar()
                        entree2503 = Entry(fenetre, textvariable=var2503, width=3)
                        entree2503.place(x=largeur + 60, y=hauteur)

                        var2504 = IntVar()
                        entree2504 = Entry(fenetre, textvariable=var2504, width=3)
                        entree2504.place(x=largeur + 90, y=hauteur)

                        var2505 = IntVar()
                        entree2505 = Entry(fenetre, textvariable=var2505, width=3)
                        entree2505.place(x=largeur + 120, y=hauteur)

                        var2506 = IntVar()
                        entree2506 = Entry(fenetre, textvariable=var2506, width=3)
                        entree2506.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["250"] = nbSeries

                    elif (discipline == 251):

                        disciplineStr = "Pistolet Percussion Centrale (251)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label251 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label251.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6
                        
                        var2511 = IntVar()
                        entree2511 = Entry(fenetre, textvariable=var2511, width=3)
                        entree2511.place(x=largeur, y=hauteur)

                        var2512 = IntVar()
                        entree2512 = Entry(fenetre, textvariable=var2512, width=3)
                        entree2512.place(x=largeur + 30, y=hauteur)

                        var2513 = IntVar()
                        entree2513 = Entry(fenetre, textvariable=var2513, width=3)
                        entree2513.place(x=largeur + 60, y=hauteur)

                        var2514 = IntVar()
                        entree2514 = Entry(fenetre, textvariable=var2514, width=3)
                        entree2514.place(x=largeur + 90, y=hauteur)

                        var2515 = IntVar()
                        entree2515 = Entry(fenetre, textvariable=var2515, width=3)
                        entree2515.place(x=largeur + 120, y=hauteur)

                        var2516 = IntVar()
                        entree2516 = Entry(fenetre, textvariable=var2516, width=3)
                        entree2516.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["251"] = nbSeries

                    elif (discipline == 252):

                        disciplineStr = "Pistolet Standard 25m (252)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label252 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label252.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6
                            
                        var2521 = IntVar()
                        entree2521 = Entry(fenetre, textvariable=var2521, width=3)
                        entree2521.place(x=largeur, y=hauteur)

                        var2522 = IntVar()
                        entree2522 = Entry(fenetre, textvariable=var2522, width=3)
                        entree2522.place(x=largeur + 30, y=hauteur)

                        var2523 = IntVar()
                        entree2523 = Entry(fenetre, textvariable=var2523, width=3)
                        entree2523.place(x=largeur + 60, y=hauteur)

                        var2524 = IntVar()
                        entree2524 = Entry(fenetre, textvariable=var2524, width=3)
                        entree2524.place(x=largeur + 90, y=hauteur)

                        var2525 = IntVar()
                        entree2525 = Entry(fenetre, textvariable=var2525, width=3)
                        entree2525.place(x=largeur + 120, y=hauteur)

                        var2526 = IntVar()
                        entree2526 = Entry(fenetre, textvariable=var2526, width=3)
                        entree2526.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["252"] = nbSeries

                    elif (discipline == 253):

                        disciplineStr = "Pistolet Vitesse Olympique (253)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label253 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label253.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                            nbSeries = 6
                            
                        var2531 = IntVar()
                        entree2531 = Entry(fenetre, textvariable=var2531, width=3)
                        entree2531.place(x=largeur, y=hauteur)

                        var2532 = IntVar()
                        entree2532 = Entry(fenetre, textvariable=var2532, width=3)
                        entree2532.place(x=largeur + 30, y=hauteur)

                        var2533 = IntVar()
                        entree2533 = Entry(fenetre, textvariable=var2533, width=3)
                        entree2533.place(x=largeur + 60, y=hauteur)

                        var2534 = IntVar()
                        entree2534 = Entry(fenetre, textvariable=var2534, width=3)
                        entree2534.place(x=largeur + 90, y=hauteur)

                        var2535 = IntVar()
                        entree2535 = Entry(fenetre, textvariable=var2535, width=3)
                        entree2535.place(x=largeur + 120, y=hauteur)

                        var2536 = IntVar()
                        entree2536 = Entry(fenetre, textvariable=var2536, width=3)
                        entree2536.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["253"] = nbSeries

                    elif (discipline == 501):

                        disciplineStr = "Carabine 50m 60 Balles Couchées (501)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label501 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label501.place(x=largeur, y=hauteur)
                                            
                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                        
                        nbSeries = 6

                        var5011 = DoubleVar()
                        entree5011 = Entry(fenetre, textvariable=var5011, width=4)
                        entree5011.place(x=largeur, y=hauteur)

                        var5012 = DoubleVar()
                        entree5012 = Entry(fenetre, textvariable=var5012, width=4)
                        entree5012.place(x=largeur + 30, y=hauteur)

                        var5013 = DoubleVar()
                        entree5013 = Entry(fenetre, textvariable=var5013, width=4)
                        entree5013.place(x=largeur + 60, y=hauteur)

                        var5014 = DoubleVar()
                        entree5014 = Entry(fenetre, textvariable=var5014, width=4)
                        entree5014.place(x=largeur + 90, y=hauteur)

                        var5015 = DoubleVar()
                        entree5015 = Entry(fenetre, textvariable=var5015, width=4)
                        entree5015.place(x=largeur + 120, y=hauteur)

                        var5016 = DoubleVar()
                        entree5016 = Entry(fenetre, textvariable=var5016, width=4)
                        entree5016.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["501"] = nbSeries

                        series = []

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE501 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE501.place(x=largeur, y=hauteur)


                        labelPE5012 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE5012.place(x=largeur, y=hauteur + 25)
                        
                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        varPE5011 = IntVar()
                        entreePE5011 = Entry(fenetre, textvariable=varPE5011, width=3)
                        entreePE5011.place(x=largeur, y=hauteur)

                        varPE5012 = IntVar()
                        entreePE5012 = Entry(fenetre, textvariable=varPE5012, width=3)
                        entreePE5012.place(x=largeur + 30, y=hauteur)

                        varPE5013 = IntVar()
                        entreePE5013 = Entry(fenetre, textvariable=varPE5013, width=3)
                        entreePE5013.place(x=largeur + 60, y=hauteur)

                        varPE5014 = IntVar()
                        entreePE5014 = Entry(fenetre, textvariable=varPE5014, width=3)
                        entreePE5014.place(x=largeur + 90, y=hauteur)

                        varPE5015 = IntVar()
                        entreePE5015 = Entry(fenetre, textvariable=varPE5015, width=3)
                        entreePE5015.place(x=largeur + 120, y=hauteur)

                        varPE5016 = IntVar()
                        entreePE5016 = Entry(fenetre, textvariable=varPE5016, width=3)
                        entreePE5016.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                    
                    elif (discipline == 831) :

                        disciplineStr = "Pistolet Vitesse Réglementaire (831)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label831 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label831.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                        
                        nbSeries = 2

                        var8311 = IntVar()
                        entree8311 = Entry(fenetre, textvariable=var8311, width=3)
                        entree8311.place(x=largeur, y=hauteur)

                        var8312 = IntVar()
                        entree8312 = Entry(fenetre, textvariable=var8312, width=3)
                        entree8312.place(x=largeur + 30, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["831"] = nbSeries

                    elif (discipline == 996):

                        disciplineStr = "Carabine Rimfire (996)"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label996 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label996.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                            nbSeries = 3
                        
                        var9961 = IntVar()
                        entree9961 = Entry(fenetre, textvariable=var9961, width=3)
                        entree9961.place(x=largeur, y=hauteur)

                        var9962 = IntVar()
                        entree9962 = Entry(fenetre, textvariable=var9962, width=3)
                        entree9962.place(x=largeur + 30, y=hauteur)

                        var9963 = IntVar()
                        entree9963 = Entry(fenetre, textvariable=var9963, width=3)
                        entree9963.place(x=largeur + 60, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["996"] = nbSeries

                    elif (discipline == 171):

                        disciplineStr = "Carabine 10m Debout SH1 Hommes"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label171 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label171.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6
                        
                        var1711 = DoubleVar()
                        entree1711 = Entry(fenetre, textvariable=var1711, width=3)
                        entree1711.place(x=largeur, y=hauteur)

                        var1712 = DoubleVar()
                        entree1712 = Entry(fenetre, textvariable=var1712, width=3)
                        entree1712.place(x=largeur + 30, y=hauteur)

                        var1713 = DoubleVar()
                        entree1713 = Entry(fenetre, textvariable=var1713, width=3)
                        entree1713.place(x=largeur + 60, y=hauteur)

                        var1714 = DoubleVar()
                        entree1714 = Entry(fenetre, textvariable=var1714, width=3)
                        entree1714.place(x=largeur + 90, y=hauteur)

                        var1715 = DoubleVar()
                        entree1715 = Entry(fenetre, textvariable=var1715, width=3)
                        entree1715.place(x=largeur + 120, y=hauteur)

                        var1716 = DoubleVar()
                        entree1716 = Entry(fenetre, textvariable=var1716, width=3)
                        entree1716.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["171"] = nbSeries

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE171 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE171.place(x=largeur, y=hauteur)

                        labelPE1712 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE1712.place(x=largeur, y=hauteur + 25)
                        
                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        varPE1711 = IntVar()
                        entreePE1711 = Entry(fenetre, textvariable=varPE1711, width=3)
                        entreePE1711.place(x=largeur, y=hauteur)

                        varPE1712 = IntVar()
                        entreePE1712 = Entry(fenetre, textvariable=varPE1712, width=3)
                        entreePE1712.place(x=largeur + 30, y=hauteur)

                        varPE1713 = IntVar()
                        entreePE1713 = Entry(fenetre, textvariable=varPE1713, width=3)
                        entreePE1713.place(x=largeur + 60, y=hauteur)

                        varPE1714 = IntVar()
                        entreePE1714 = Entry(fenetre, textvariable=varPE1714, width=3)
                        entreePE1714.place(x=largeur + 90, y=hauteur)

                        varPE1715 = IntVar()
                        entreePE1715 = Entry(fenetre, textvariable=varPE1715, width=3)
                        entreePE1715.place(x=largeur + 120, y=hauteur)

                        varPE1716 = IntVar()
                        entreePE1716 = Entry(fenetre, textvariable=varPE1716, width=3)
                        entreePE1716.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350
                    
                    elif (discipline == 172):

                        disciplineStr = "Carabine 10m Debout SH1 Dames"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label172 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label172.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6
                        
                        var1721 = DoubleVar()
                        entree1721 = Entry(fenetre, textvariable=var1721, width=3)
                        entree1721.place(x=largeur, y=hauteur)

                        var1722 = DoubleVar()
                        entree1722 = Entry(fenetre, textvariable=var1722, width=3)
                        entree1722.place(x=largeur + 30, y=hauteur)

                        var1723 = DoubleVar()
                        entree1723 = Entry(fenetre, textvariable=var1723, width=3)
                        entree1723.place(x=largeur + 60, y=hauteur)

                        var1724 = DoubleVar()
                        entree1724 = Entry(fenetre, textvariable=var1724, width=3)
                        entree1724.place(x=largeur + 90, y=hauteur)

                        var1725 = DoubleVar()
                        entree1725 = Entry(fenetre, textvariable=var1725, width=3)
                        entree1725.place(x=largeur + 120, y=hauteur)

                        var1726 = DoubleVar()
                        entree1726 = Entry(fenetre, textvariable=var1726, width=3)
                        entree1726.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["172"] = nbSeries

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE172 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE172.place(x=largeur, y=hauteur)

                        labelPE1722 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE1722.place(x=largeur, y=hauteur + 25)
                        
                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        varPE1721 = IntVar()
                        entreePE1721 = Entry(fenetre, textvariable=varPE1721, width=3)
                        entreePE1721.place(x=largeur, y=hauteur)

                        varPE1722 = IntVar()
                        entreePE1722 = Entry(fenetre, textvariable=varPE1722, width=3)
                        entreePE1722.place(x=largeur + 30, y=hauteur)

                        varPE1723 = IntVar()
                        entreePE1723 = Entry(fenetre, textvariable=varPE1723, width=3)
                        entreePE1723.place(x=largeur + 60, y=hauteur)

                        varPE1724 = IntVar()
                        entreePE1724 = Entry(fenetre, textvariable=varPE1724, width=3)
                        entreePE1724.place(x=largeur + 90, y=hauteur)

                        varPE1725 = IntVar()
                        entreePE1725 = Entry(fenetre, textvariable=varPE1725, width=3)
                        entreePE1725.place(x=largeur + 120, y=hauteur)

                        varPE1726 = IntVar()
                        entreePE1726 = Entry(fenetre, textvariable=varPE1726, width=3)
                        entreePE1726.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 174):

                        disciplineStr = "Carabine 10m Debout SH2 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label174 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label174.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var1741 = DoubleVar()
                        entree1741 = Entry(fenetre, textvariable=var1741, width=3)
                        entree1741.place(x=largeur, y=hauteur)

                        var1742 = DoubleVar()
                        entree1742 = Entry(fenetre, textvariable=var1742, width=3)
                        entree1742.place(x=largeur + 30, y=hauteur)

                        var1743 = DoubleVar()
                        entree1743 = Entry(fenetre, textvariable=var1743, width=3)
                        entree1743.place(x=largeur + 60, y=hauteur)

                        var1744 = DoubleVar()
                        entree1744 = Entry(fenetre, textvariable=var1744, width=3)
                        entree1744.place(x=largeur + 90, y=hauteur)

                        var1745 = DoubleVar()
                        entree1745 = Entry(fenetre, textvariable=var1745, width=3)
                        entree1745.place(x=largeur + 120, y=hauteur)

                        var1746 = DoubleVar()
                        entree1746 = Entry(fenetre, textvariable=var1746, width=3)
                        entree1746.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["174"] = nbSeries

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE174 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE174.place(x=largeur, y=hauteur)

                        labelPE1742 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE1742.place(x=largeur, y=hauteur + 25)

                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        varPE1741 = IntVar()
                        entreePE1741 = Entry(fenetre, textvariable=varPE1741, width=3)
                        entreePE1741.place(x=largeur, y=hauteur)

                        varPE1742 = IntVar()
                        entreePE1742 = Entry(fenetre, textvariable=varPE1742, width=3)
                        entreePE1742.place(x=largeur + 30, y=hauteur)

                        varPE1743 = IntVar()
                        entreePE1743 = Entry(fenetre, textvariable=varPE1743, width=3)
                        entreePE1743.place(x=largeur + 60, y=hauteur)

                        varPE1744 = IntVar()
                        entreePE1744 = Entry(fenetre, textvariable=varPE1744, width=3)
                        entreePE1744.place(x=largeur + 90, y=hauteur)

                        varPE1745 = IntVar()
                        entreePE1745 = Entry(fenetre, textvariable=varPE1745, width=3)
                        entreePE1745.place(x=largeur + 120, y=hauteur)

                        varPE1746 = IntVar()
                        entreePE1746 = Entry(fenetre, textvariable=varPE1746, width=3)
                        entreePE1746.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 181):

                        disciplineStr = "Pistolet 10m SH1 Hommes"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label181 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label181.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var1811 = IntVar()
                        entree1811 = Entry(fenetre, textvariable=var1811, width=3)
                        entree1811.place(x=largeur, y=hauteur)

                        var1812 = IntVar()
                        entree1812 = Entry(fenetre, textvariable=var1812, width=3)
                        entree1812.place(x=largeur + 30, y=hauteur)

                        var1813 = IntVar()
                        entree1813 = Entry(fenetre, textvariable=var1813, width=3)
                        entree1813.place(x=largeur + 60, y=hauteur)

                        var1814 = IntVar()
                        entree1814 = Entry(fenetre, textvariable=var1814, width=3)
                        entree1814.place(x=largeur + 90, y=hauteur)

                        var1815 = IntVar()
                        entree1815 = Entry(fenetre, textvariable=var1815, width=3)
                        entree1815.place(x=largeur + 120, y=hauteur)

                        var1816 = IntVar()
                        entree1816 = Entry(fenetre, textvariable=var1816, width=3)
                        entree1816.place(x=largeur + 150, y=hauteur)

                        ordre_dict["181"] = nbSeries

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 182):

                        disciplineStr = "Pistolet 10m SH1 Dames"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label182 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label182.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var1821 = IntVar()
                        entree1821 = Entry(fenetre, textvariable=var1821, width=3)
                        entree1821.place(x=largeur, y=hauteur)

                        var1822 = IntVar()
                        entree1822 = Entry(fenetre, textvariable=var1822, width=3)
                        entree1822.place(x=largeur + 30, y=hauteur)

                        var1823 = IntVar()
                        entree1823 = Entry(fenetre, textvariable=var1823, width=3)
                        entree1823.place(x=largeur + 60, y=hauteur)

                        var1824 = IntVar()
                        entree1824 = Entry(fenetre, textvariable=var1824, width=3)
                        entree1824.place(x=largeur + 90, y=hauteur)

                        var1825 = IntVar()
                        entree1825 = Entry(fenetre, textvariable=var1825, width=3)
                        entree1825.place(x=largeur + 120, y=hauteur)

                        var1826 = IntVar()
                        entree1826 = Entry(fenetre, textvariable=var1826, width=3)
                        entree1826.place(x=largeur + 150, y=hauteur)

                        ordre_dict["182"] = nbSeries
                        
                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 183):

                        disciplineStr = "Pistolet Standard 10m SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label183 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label183.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 4

                        var1831 = IntVar()
                        entree1831 = Entry(fenetre, textvariable=var1831, width=3)
                        entree1831.place(x=largeur, y=hauteur)

                        var1832 = IntVar()
                        entree1832 = Entry(fenetre, textvariable=var1832, width=3)
                        entree1832.place(x=largeur + 30, y=hauteur)

                        var1833 = IntVar()
                        entree1833 = Entry(fenetre, textvariable=var1833, width=3)
                        entree1833.place(x=largeur + 60, y=hauteur)

                        var1834 = IntVar()
                        entree1834 = Entry(fenetre, textvariable=var1834, width=3)
                        entree1834.place(x=largeur + 90, y=hauteur)

                        ordre_dict["183"] = nbSeries

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 184):

                        disciplineStr = "Pistolet Vitesse 10m SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label184 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label184.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 8

                        var1841 = IntVar()
                        entree1841 = Entry(fenetre, textvariable=var1841, width=3)
                        entree1841.place(x=largeur, y=hauteur)

                        var1842 = IntVar()
                        entree1842 = Entry(fenetre, textvariable=var1842, width=3)
                        entree1842.place(x=largeur + 30, y=hauteur)

                        var1843 = IntVar()
                        entree1843 = Entry(fenetre, textvariable=var1843, width=3)
                        entree1843.place(x=largeur + 60, y=hauteur)

                        var1844 = IntVar()
                        entree1844 = Entry(fenetre, textvariable=var1844, width=3)
                        entree1844.place(x=largeur + 90, y=hauteur)

                        var1845 = IntVar()
                        entree1845 = Entry(fenetre, textvariable=var1845, width=3)
                        entree1845.place(x=largeur + 120, y=hauteur)

                        var1846 = IntVar()
                        entree1846 = Entry(fenetre, textvariable=var1846, width=3)
                        entree1846.place(x=largeur + 150, y=hauteur)

                        var1847 = IntVar()
                        entree1847 = Entry(fenetre, textvariable=var1847, width=3)
                        entree1847.place(x=largeur + 180, y=hauteur)

                        var1848= IntVar()
                        entree1848 = Entry(fenetre, textvariable=var1848, width=3)
                        entree1848.place(x=largeur + 210, y=hauteur)

                        ordre_dict["184"] = nbSeries

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 271):

                        disciplineStr = "Pistolet 25m SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label271 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label271.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var2711 = IntVar()
                        entree2711 = Entry(fenetre, textvariable=var2711, width=3)
                        entree2711.place(x=largeur, y=hauteur)

                        var2712 = IntVar()
                        entree2712 = Entry(fenetre, textvariable=var2712, width=3)
                        entree2712.place(x=largeur + 30, y=hauteur)

                        var2713 = IntVar()
                        entree2713 = Entry(fenetre, textvariable=var2713, width=3)
                        entree2713.place(x=largeur + 60, y=hauteur)

                        var2714 = IntVar()
                        entree2714 = Entry(fenetre, textvariable=var2714, width=3)
                        entree2714.place(x=largeur + 90, y=hauteur)

                        var2715 = IntVar()
                        entree2715 = Entry(fenetre, textvariable=var2715, width=3)
                        entree2715.place(x=largeur + 120, y=hauteur)

                        var2716 = IntVar()
                        entree2716 = Entry(fenetre, textvariable=var2716, width=3)
                        entree2716.place(x=largeur + 150, y=hauteur)

                        ordre_dict["271"] = nbSeries

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 273):

                        disciplineStr = "Pistolet Standard 25m SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label273 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label273.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var2731 = IntVar()
                        entree2731 = Entry(fenetre, textvariable=var2731, width=3)
                        entree2731.place(x=largeur, y=hauteur)

                        var2732 = IntVar()
                        entree2732 = Entry(fenetre, textvariable=var2732, width=3)
                        entree2732.place(x=largeur + 30, y=hauteur)

                        var2733 = IntVar()
                        entree2733 = Entry(fenetre, textvariable=var2733, width=3)
                        entree2733.place(x=largeur + 60, y=hauteur)

                        var2734 = IntVar()
                        entree2734 = Entry(fenetre, textvariable=var2734, width=3)
                        entree2734.place(x=largeur + 90, y=hauteur)

                        var2735 = IntVar()
                        entree2735 = Entry(fenetre, textvariable=var2735, width=3)
                        entree2735.place(x=largeur + 120, y=hauteur)

                        var2736 = IntVar()
                        entree2736 = Entry(fenetre, textvariable=var2736, width=3)
                        entree2736.place(x=largeur + 150, y=hauteur)

                        ordre_dict["273"] = nbSeries

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 274):

                        disciplineStr = "Pistolet Percussion Centrale SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label274 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label274.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var2741 = IntVar()
                        entree2741 = Entry(fenetre, textvariable=var2741, width=3)
                        entree2741.place(x=largeur, y=hauteur)

                        var2742 = IntVar()
                        entree2742 = Entry(fenetre, textvariable=var2742, width=3)
                        entree2742.place(x=largeur + 30, y=hauteur)

                        var2743 = IntVar()
                        entree2743 = Entry(fenetre, textvariable=var2743, width=3)
                        entree2743.place(x=largeur + 60, y=hauteur)

                        var2744 = IntVar()
                        entree2744 = Entry(fenetre, textvariable=var2744, width=3)
                        entree2744.place(x=largeur + 90, y=hauteur)

                        var2745 = IntVar()
                        entree2745 = Entry(fenetre, textvariable=var2745, width=3)
                        entree2745.place(x=largeur + 120, y=hauteur)

                        var2746 = IntVar()
                        entree2746 = Entry(fenetre, textvariable=var2746, width=3)
                        entree2746.place(x=largeur + 150, y=hauteur)

                        ordre_dict["274"] = nbSeries

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 571):

                        disciplineStr = "Carabine 50m 60 Balles Couchées SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label571 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label571.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var5711 = DoubleVar()
                        entree5711 = Entry(fenetre, textvariable=var5711, width=3)
                        entree5711.place(x=largeur, y=hauteur)

                        var5712 = DoubleVar()
                        entree5712 = Entry(fenetre, textvariable=var5712, width=3)
                        entree5712.place(x=largeur + 30, y=hauteur)

                        var5713 = DoubleVar()
                        entree5713 = Entry(fenetre, textvariable=var5713, width=3)
                        entree5713.place(x=largeur + 60, y=hauteur)

                        var5714 = DoubleVar()
                        entree5714 = Entry(fenetre, textvariable=var5714, width=3)
                        entree5714.place(x=largeur + 90, y=hauteur)

                        var5715 = DoubleVar()
                        entree5715 = Entry(fenetre, textvariable=var5715, width=3)
                        entree5715.place(x=largeur + 120, y=hauteur)

                        var5716 = DoubleVar()
                        entree5716 = Entry(fenetre, textvariable=var5716, width=3)
                        entree5716.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["571"] = nbSeries

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE571 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE571.place(x=largeur, y=hauteur)

                        labelPE5712 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE5712.place(x=largeur, y=hauteur + 25)

                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        varPE5711 = IntVar()
                        entreePE5711 = Entry(fenetre, textvariable=varPE5711, width=3)
                        entreePE5711.place(x=largeur, y=hauteur)

                        varPE5712 = IntVar()
                        entreePE5712 = Entry(fenetre, textvariable=varPE5712, width=3)
                        entreePE5712.place(x=largeur + 30, y=hauteur)

                        varPE5713 = IntVar()
                        entreePE5713 = Entry(fenetre, textvariable=varPE5713, width=3)
                        entreePE5713.place(x=largeur + 60, y=hauteur)

                        varPE5714 = IntVar()
                        entreePE5714 = Entry(fenetre, textvariable=varPE5714, width=3)
                        entreePE5714.place(x=largeur + 90, y=hauteur)

                        varPE5715 = IntVar()
                        entreePE5715 = Entry(fenetre, textvariable=varPE5715, width=3)
                        entreePE5715.place(x=largeur + 120, y=hauteur)

                        varPE5716 = IntVar()
                        entreePE5716 = Entry(fenetre, textvariable=varPE5716, width=3)
                        entreePE5716.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                    elif (discipline == 574):

                        disciplineStr = "Carabine 50m 60 Balles Couchées SH1 Mixte"

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        label574 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        label574.place(x=largeur, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        nbSeries = 6

                        var5741 = DoubleVar()
                        entree5741 = Entry(fenetre, textvariable=var5741, width=3)
                        entree5741.place(x=largeur, y=hauteur)

                        var5742 = DoubleVar()
                        entree5742 = Entry(fenetre, textvariable=var5742, width=3)
                        entree5742.place(x=largeur + 30, y=hauteur)

                        var5743 = DoubleVar()
                        entree5743 = Entry(fenetre, textvariable=var5743, width=3)
                        entree5743.place(x=largeur + 60, y=hauteur)

                        var5744 = DoubleVar()
                        entree5744 = Entry(fenetre, textvariable=var5744, width=3)
                        entree5744.place(x=largeur + 90, y=hauteur)

                        var5745 = DoubleVar()
                        entree5745 = Entry(fenetre, textvariable=var5745, width=3)
                        entree5745.place(x=largeur + 120, y=hauteur)

                        var5746 = DoubleVar()
                        entree5746 = Entry(fenetre, textvariable=var5746, width=3)
                        entree5746.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                        ordre_dict["574"] = nbSeries

                        if (hauteur > 450):
                            hauteur = 50
                            largeur += 350

                        labelPE574 = Label(fenetre, text="Ajouter Séries de " + disciplineStr)
                        labelPE574.place(x=largeur, y=hauteur)

                        labelPE5742 = Label(fenetre, text="(Au Points Entiers)")
                        labelPE5742.place(x=largeur, y=hauteur + 25)

                        if (hauteur <= 450):
                            hauteur += 50
                        else:
                            hauteur = 50
                            largeur += 350

                        varPE5741 = IntVar()
                        entreePE5741 = Entry(fenetre, textvariable=varPE5741, width=3)
                        entreePE5741.place(x=largeur, y=hauteur)

                        varPE5742 = IntVar()
                        entreePE5742 = Entry(fenetre, textvariable=varPE5742, width=3)
                        entreePE5742.place(x=largeur + 30, y=hauteur)

                        varPE5743 = IntVar()
                        entreePE5743 = Entry(fenetre, textvariable=varPE5743, width=3)
                        entreePE5743.place(x=largeur + 60, y=hauteur)

                        varPE5744 = IntVar()
                        entreePE5744 = Entry(fenetre, textvariable=varPE5744, width=3)
                        entreePE5744.place(x=largeur + 90, y=hauteur)

                        varPE5745 = IntVar()
                        entreePE5745 = Entry(fenetre, textvariable=varPE5745, width=3)
                        entreePE5745.place(x=largeur + 120, y=hauteur)

                        varPE5746 = IntVar()
                        entreePE5746 = Entry(fenetre, textvariable=varPE5746, width=3)
                        entreePE5746.place(x=largeur + 150, y=hauteur)

                        if (hauteur <= 475):
                            hauteur += 25
                        else:
                            hauteur = 50
                            largeur += 350

                if (hauteur <= 475):
                    hauteur += 25
                else:
                    hauteur = 50
                    largeur += 350

                boutonAjouterSerie = Button(fenetre, text="Ajouter Séries", height=3, width=20, command=envoyer_series)
                boutonAjouterSerie.place(x=largeur, y=hauteur)

                boutonRetourSeries = Button(fenetre, text="Retour", height=3, width=20, command=retour_series)
                boutonRetourSeries.place(x=largeur + 200, y=hauteur)

                if (hauteur <= 425):
                    hauteur += 75
                else:
                    hauteur = 50
                    largeur += 350

                infosLabel = Label(fenetre, text="(Ne pas modifier les séries non-tirées OU déjà rentrées !)")
                infosLabel.place(x=largeur, y=hauteur)

    def retour_participant_series():
        demanderParticipant.destroy()
        nomText.destroy()
        nomEntree.destroy()
        prenomText.destroy()
        prenomEntree.destroy()
        boutonValider.destroy()
        boutonRetour.destroy()

        fenetre_principale()

    titreSeries = Label(fenetre, text="Ajouter Séries")
    titreSeries.place(x=350, y=10)

    demanderParticipant = Label(fenetre, text="À qui souhaitez-vous ajouter des séries ?")
    demanderParticipant.place(x=300, y=50)

    nomText = Label(fenetre, text="Nom")
    nomText.place(x=300, y=75)

    nomVar = StringVar()
    nomEntree = Entry(fenetre, textvariable=nomVar, width=30)
    nomEntree.place(x=300, y=100)

    prenomText = Label(fenetre, text="Prénom")
    prenomText.place(x=300, y=125)

    prenomVar = StringVar()
    prenomEntree = Entry(fenetre, textvariable=prenomVar, width=30)
    prenomEntree.place(x=300, y=150)

    boutonValider = Button(fenetre, text="Valider", height=3, width=20, command=definir_series)
    boutonValider.place(x=225, y=200)

    boutonRetour = Button(fenetre, text="Retour", height=3, width=20, command=retour_participant_series)
    boutonRetour.place(x=425, y=200)

def creer_pdf():
    global participants
    global disciplinesList
    global challengeEquipes

    hauteur = 28

    creer_equipe()
    creer_equipes_challenge()

    canvas = cv("Palmares.pdf", pagesize=A4)

    canvas.setFont("Helvetica-Bold", 16)
    canvas.drawString(5 * cm, hauteur * cm, "CHALLENGE CHOLET TIR SPORTIF 2022")

    hauteur -= 1
    canvas.drawString(9 * cm, hauteur * cm, "PALMARES")
    hauteur -= 1
    canvas.drawImage("logo.jpg", 17 * cm , hauteur * cm, width=96, height=96, mask=None)
    hauteur -= 1

    canvas.setFont("Helvetica-Bold", 14)
    canvas.drawString(2 * cm, hauteur * cm, "Palmares Individuels")

    canvas.setFont("Helvetica", 8)

    for discipline in disciplinesList:
        if (discipline == 100):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet 10m"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["100total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(9 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["PF", "PG", "BF", "BG", "MF", "MG", "CF", "CG", "JF", "JG", "D1", "D2", "D3", "S1", "S2", "S3"]
            
            for categorie in categoriesList:

                if (categorie == "PG"): categorieStr = "Poussins Garçons" 
                elif (categorie == "PF"): categorieStr = "Poussins Filles"
                elif (categorie == "BG"): categorieStr = "Benjamins Garçons"
                elif (categorie == "BF"): categorieStr = "Benjamins Filles"
                elif (categorie == "MG"): categorieStr = "Minimes Garçons"
                elif (categorie == "MF"): categorieStr = "Minimes Filles"
                elif (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "CF"): categorieStr = "Cadets Filles"
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "JF"): categorieStr = "Juniors Filles"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "D3"): categorieStr = "Dames 3"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if ((participant["categorie"] == categorie) and (discipline in participant["disciplines"]) and (participant["100total"] != -1)):
                        participantsRetenus.append(participant)
                
                if (len(participantsRetenus) > 0):
                    
                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5

                    participantsTries = sorted(participantsRetenus, key=itemgetter('100total'), reverse=True)

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["100"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["100total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5

        elif (discipline == 102):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Vitesse 10m"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["102total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["MF", "MG", "CF", "CG", "JF", "JG", "D1", "D2", "S1", "S2", "S3"]
            
            for categorie in categoriesList:
                
                if (categorie == "MG"): categorieStr = "Minimes Garçons" 
                elif (categorie == "MF"): categorieStr = "Minimes Filles"
                elif (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "CF"): categorieStr = "Cadets Filles"
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "JF"): categorieStr = "Juniors Filles"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "D3") and (categorie == "D2"))) and (discipline in participant["disciplines"]) and (participant["102total"] != -1)):
                        participantsRetenus.append(participant)
                
                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5

                    participantsTries = sorted(participantsRetenus, key=itemgetter('102total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["102"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["102total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 103):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28
            disciplineStr = "Pistolet Standard 10m"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["103total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["CG", "JF", "JG", "D1", "D2", "S1", "S2", "S3"]

            for categorie in categoriesList:

                if (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "JF"): categorieStr = "Juniors Filles"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "CF") and (categorie == "CG")) or ((participant["categorie"] == "D3") and (categorie == "D2")) or ((participant["categorie"] == "MG") and (categorie == "CG")) or ((participant["categorie"] == "MF") and (categorie == "CG"))) and (discipline in participant["disciplines"]) and (participant["103total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('103total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["103"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["103total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 104):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28
            disciplineStr = "Carabine 10m"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["104total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(9 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["PF", "PG", "BF", "BG", "MF", "MG", "CF", "CG", "JF", "JG", "D1", "D2", "D3", "S1", "S2", "S3", "PB", "PM", "PM21", "PP21"]

            for categorie in categoriesList:

                if (categorie == "PG"): categorieStr = "Poussins Garçons" 
                elif (categorie == "PF"): categorieStr = "Poussins Filles"
                elif (categorie == "BG"): categorieStr = "Benjamins Garçons"
                elif (categorie == "BF"): categorieStr = "Benjamins Filles"
                elif (categorie == "MG"): categorieStr = "Minimes Garçons"
                elif (categorie == "MF"): categorieStr = "Minimes Filles"
                elif (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "CF"): categorieStr = "Cadets Filles"
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "JF"): categorieStr = "Juniors Filles"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "D3"): categorieStr = "Dames 3"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"
                elif (categorie == "PB"): categorieStr = "Potence Benjamin"
                elif (categorie == "PM"): categorieStr = "Potence Minime"
                elif (categorie == "PM21"): categorieStr = "Potence Moins de 21ans"
                elif (categorie == "PP21"): categorieStr = "Potence Plus de 21ans"

                participantsRetenus = []

                for participant in participants:
                    if ((participant["categorie"] == categorie) and (discipline in participant["disciplines"]) and (participant["104total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('104total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["104"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["104total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 250):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet 25m"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["250total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(9 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["CF", "CG", "JF", "JG", "D1", "D2", "S1", "S2", "S3"]

            for categorie in categoriesList:

                if (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "CF"): categorieStr = "Cadets Filles"
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "JF"): categorieStr = "Juniors Filles"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "D3") and (categorie == "D2"))) and (discipline in participant["disciplines"]) and (participant["250total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('250total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr1 = "Precision : "
                        seriesStr2 = "Vitesse : "
                        total1 = 0
                        total2 = 0
                        j = 0
                        for val in participant["250"]:
                            if (j < 3):
                                seriesStr1 += str(val) + " "
                                total1 += val
                            else: 
                                seriesStr2 += str(val) + " "
                                total2 += val
                            j += 1
                        seriesStr1 += "    " + str(total1)
                        seriesStr2 += "    " + str(total2)

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(12.8 * cm, hauteur * cm, seriesStr1)
                        canvas.drawString(13 * cm, (hauteur - 0.5) * cm, seriesStr2)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["250total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 1
        elif (discipline == 251):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Percussion Centrale"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["251total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7.5 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["CF", "CG", "JF", "JG", "D1", "D2", "S1", "S2", "S3"]

            for categorie in categoriesList:

                if (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "D3") and (categorie == "D2"))) and (discipline in participant["disciplines"]) and (participant["251total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('251total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["251"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["251total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 252):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Standard 25m"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["252total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["CG", "JG", "D1", "D2", "S1", "S2", "S3"]

            for categorie in categoriesList:

                if (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "CF") and (categorie == "CG")) or ((participant["categorie"] == "JF") and (categorie == "JG")) or ((participant["categorie"] == "D3") and (categorie == "D2"))) and (discipline in participant["disciplines"]) and (participant["252total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('252total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["252"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["252total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 253):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Vitesse Olympique"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["253total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["CG", "JG", "S1", "S2"]

            for categorie in categoriesList:

                if (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "CF") and (categorie == "CG")) or ((participant["categorie"] == "JF") and (categorie == "JG")) or ((participant["categorie"] == "S3") and (categorie == "S2")) or ((participant["categorie"] == "D1") and (categorie == "S1")) or ((participant["categorie"] == "D2") and (categorie == "S2")) or ((participant["categorie"] == "D3") and (categorie == "S2"))) and (discipline in participant["disciplines"]) and (participant["253total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('253total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["253"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["253total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 501):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28
            disciplineStr = "Carabine 50m 60 Balles Couchées"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["501total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7.5 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["MF", "MG", "CF", "CG", "JF", "JG", "D1", "D2", "S1", "S2", "S3"]

            for categorie in categoriesList:

                if (categorie == "MG"): categorieStr = "Minimes Garçons"
                elif (categorie == "MF"): categorieStr = "Minimes Filles"
                elif (categorie == "CG"): categorieStr = "Cadets Garçons" 
                elif (categorie == "CF"): categorieStr = "Cadets Filles"
                elif (categorie == "JG"): categorieStr = "Juniors Garçons"
                elif (categorie == "JF"): categorieStr = "Juniors Filles"
                elif (categorie == "D1"): categorieStr = "Dames 1"
                elif (categorie == "D2"): categorieStr = "Dames 2"
                elif (categorie == "S1"): categorieStr = "Seniors 1"
                elif (categorie == "S2"): categorieStr = "Seniors 2"
                elif (categorie == "S3"): categorieStr = "Seniors 3"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "D3") and (categorie == "D2"))) and (discipline in participant["disciplines"]) and (participant["501total"] != -1)):
                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('501total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["501"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["501total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 831):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Vitesse Réglementaire"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["831total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["S1"]

            for categorie in categoriesList:

                if (categorie == "S1"): categorieStr = "Seniors 1"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "S2") and (categorie == "S1")) or ((participant["categorie"] == "S3") and (categorie == "S1")) or ((participant["categorie"] == "D1") and (categorie == "S1")) or ((participant["categorie"] == "D2") and (categorie == "S1")) or ((participant["categorie"] == "D3") and (categorie == "S1"))) and (discipline in participant["disciplines"]) and (participant["831total"] != -1)):

                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('831total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["831"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["831total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5
        elif (discipline == 996):
            if hauteur <= 4 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Carabine Rimfire"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["996total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(9 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            categoriesList = ["S1"]

            for categorie in categoriesList:

                if (categorie == "S1"): categorieStr = "Seniors 1"

                participantsRetenus = []

                for participant in participants:
                    if (((participant["categorie"] == categorie) or ((participant["categorie"] == "S2") and (categorie == "S1")) or ((participant["categorie"] == "S3") and (categorie == "S1")) or ((participant["categorie"] == "D1") and (categorie == "S1")) or ((participant["categorie"] == "D2") and (categorie == "S1")) or ((participant["categorie"] == "D3") and (categorie == "S1"))) and (discipline in participant["disciplines"]) and (participant["996total"] != -1)):

                        participantsRetenus.append(participant)

                if (len(participantsRetenus) > 0):

                    canvas.setFont("Helvetica-Bold", 10)
                    canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                    hauteur -= 0.5
                
                    participantsTries = sorted(participantsRetenus, key=itemgetter('996total'), reverse=True) 

                    i = 1
                    for participant in participantsTries:

                        seriesStr = ""
                        for val in participant["996"]:
                            seriesStr += str(val) + " "

                        canvas.setFont("Helvetica", 8)
                        canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                        canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                        canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                        canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                        canvas.drawString(18 * cm, hauteur * cm, str(participant["996total"]))

                        i += 1

                        if hauteur <= 2 :
                            canvas.showPage()
                            hauteur = 28
                        else: hauteur -= 0.5
                    hauteur -= 0.5    
        elif (discipline == 171):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Carabine 10m Debout SH1 Hommes"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["171total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["171total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('171total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["171"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["171total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 172):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Carabine 10m Debout SH1 Dames"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["172total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["172total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('172total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["172"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["172total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 174):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Carabine 10m Debout SH2 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["174total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["174total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('174total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["174"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["174total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 181):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet 10m SH1 Hommes"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["181total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["181total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('181total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["181"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["181total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 182):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet 10m SH1 Dames"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["182total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["182total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('182total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["182"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["182total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 183):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Standard 10m SH1 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["183total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["183total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('183total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["183"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["183total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 184):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Vitesse 10m SH1 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["184total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["184total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('184total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["184"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["184total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 271):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet 25m SH1 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["271total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(8 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["271total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('271total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["271"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["271total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 273):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Standard 25m SH1 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["273total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["273total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('273total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["273"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["273total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 274):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Pistolet Standard 25m SH1 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["274total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["274total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('274total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["274"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["274total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 571):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Carabine 50m 60 Balles Couchées SH1 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["571total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["571total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('571total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["571"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["571total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5
        elif (discipline == 574):
            if hauteur <= 3.5 :
                canvas.showPage()
                hauteur = 28

            disciplineStr = "Carabine 50m 60 Balles Couchées SH2 Mixte"

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["574total"] != -1)):
                    canvas.setFont("Helvetica-Bold", 12)
                    if (hauteur <= 26): hauteur -= 1
                    canvas.drawString(7 * cm, hauteur * cm, disciplineStr)
                    hauteur -= 1
                    break

            participantsRetenus = []

            for participant in participants:
                if ((discipline in participant["disciplines"]) and (participant["574total"] != -1)):

                    participantsRetenus.append(participant)

            if (len(participantsRetenus) > 0):
            
                participantsTries = sorted(participantsRetenus, key=itemgetter('574total'), reverse=True) 

                i = 1
                for participant in participantsTries:

                    seriesStr = ""
                    for val in participant["574"]:
                        seriesStr += str(val) + " "

                    canvas.setFont("Helvetica", 8)
                    canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                    canvas.drawString(3 * cm, hauteur * cm, participant["nom"] + " " + participant["prenom"])
                    canvas.drawString(8 * cm, hauteur * cm, participant["club"])
                    canvas.drawString(13 * cm, hauteur * cm, seriesStr)
                    canvas.drawString(18 * cm, hauteur * cm, str(participant["574total"]))

                    i += 1

                    if hauteur <= 2 :
                        canvas.showPage()
                        hauteur = 28
                    else: hauteur -= 0.5
                hauteur -= 0.5

    categoriesList = ["PF", "PG", "BF", "BG", "MF", "MG", "CF", "CG", "JF", "JG", "D1", "D2", "D3", "S1", "S2", "S3", "SH1", "SH2", "PB", "PM", "PM21", "PP21"]

    hauteur -= 1
    if hauteur <= 7 :
        canvas.showPage()
        hauteur = 28

    canvas.setFont("Helvetica-Bold", 14)
    canvas.drawString(2 * cm, hauteur * cm, "Palmares Equipes")
    hauteur -= 1

    for discipline in disciplinesList:

        if (discipline == 100): disciplineStr = "Pistolet 10m" 
        elif (discipline == 102): disciplineStr = "Pistolet Vitesse 10m"
        elif (discipline == 103): disciplineStr = "Pistolet Standard 10m"
        elif (discipline == 104): disciplineStr = "Carabine 10m"
        elif (discipline == 250): disciplineStr = "Pistolet 25m"
        elif (discipline == 252): disciplineStr = "Pistolet Standard 25m"
        elif (discipline == 253): disciplineStr = "Pistolet Vitesse Olympique"
        elif (discipline == 501): disciplineStr = "Carabine 50m 60 Balles Couchées"
        elif (discipline == 831): disciplineStr = "Pistolet Vitesse Réglementaire"
        elif (discipline == 996): disciplineStr = "Carabine Rimfire"

        for equipe in equipes:
            if (discipline == equipe["discipline"]):
                
                if (hauteur <= 26): hauteur -= 1

                if (hauteur <= 5):
                    canvas.showPage()
                    hauteur = 28
                
                canvas.setFont("Helvetica-Bold", 12)
                canvas.drawString(9 * cm, hauteur * cm, disciplineStr)
                
                if (hauteur <= 4):
                    canvas.showPage()
                    hauteur = 28
                else: hauteur -= 1
                break

        for categorie in categoriesList:

            if (categorie == "PG"): categorieStr = "Poussins Garçons" 
            elif (categorie == "PF"): categorieStr = "Poussins Filles"
            elif (categorie == "BG"): categorieStr = "Benjamins Garçons"
            elif (categorie == "BF"): categorieStr = "Benjamins Filles"
            elif (categorie == "MG"): categorieStr = "Minimes Garçons"
            elif (categorie == "MF"): categorieStr = "Minimes Filles"
            elif (categorie == "CG"): categorieStr = "Cadets Garçons" 
            elif (categorie == "CF"): categorieStr = "Cadets Filles"
            elif (categorie == "JG"): categorieStr = "Juniors Garçons"
            elif (categorie == "JF"): categorieStr = "Juniors Filles"
            elif (categorie == "D1"): categorieStr = "Dames 1"
            elif (categorie == "D2"): categorieStr = "Dames 2"
            elif (categorie == "D3"): categorieStr = "Dames 3"
            elif (categorie == "S1"): categorieStr = "Seniors 1"
            elif (categorie == "S2"): categorieStr = "Seniors 2"
            elif (categorie == "S3"): categorieStr = "Seniors 3"
            elif (categorie == "SH1"): categorieStr = "Seniors Handisport 1"
            elif (categorie == "SH2"): categorieStr = "Seniors Handisport 2"
            elif (categorie == "PB"): categorieStr = "Potence Benjamin"
            elif (categorie == "PM"): categorieStr = "Potence Minime"
            elif (categorie == "PM21"): categorieStr = "Potence Moins de 21ans"
            elif (categorie == "PM21"): categorieStr = "Potence Plus de 21ans"

            equipesRetenues = []
            for equipe in equipes:
                if ((equipe["discipline"] == discipline) and (equipe["categorie"] == categorie)):
                    equipesRetenues.append(equipe)

            if (len(equipesRetenues) > 0):

                canvas.setFont("Helvetica-Bold", 10)
                canvas.drawString(2 * cm, hauteur * cm, categorieStr + " :")

                hauteur -= 0.5

            equipesTriees = sorted(equipesRetenues, key=itemgetter('total'), reverse=True)

            i = 1
            for equipe in equipesTriees:

                seriesStr1 = ""
                seriesStr2 = ""
                seriesStr3 = ""
                for val in equipe["score1"]:
                    seriesStr1+= str(val) + " "
                for val in equipe["score2"]:
                    seriesStr2+= str(val) + " "
                for val in equipe["score3"]:
                    seriesStr3+= str(val) + " "

                canvas.setFont("Helvetica", 8)
                canvas.drawString(2.2 * cm, hauteur * cm, str(i) + ". ")
                canvas.drawString(3 * cm, hauteur * cm, equipe["club"])
                canvas.drawString(7 * cm, hauteur * cm, equipe["nom1"] + " " + equipe["prenom1"])
                canvas.drawString(12 * cm, hauteur * cm, seriesStr1)
                if (equipe["discipline"] == 104 or equipe["discipline"] == 501):
                    canvas.drawString(17 * cm, hauteur * cm, str(round(equipe["total1"], 2)))
                    canvas.drawString(17 * cm, (hauteur - 0.5) * cm, str(round(equipe["total2"], 2)))
                    canvas.drawString(17 * cm, (hauteur - 1) * cm, str(round(equipe["total3"], 2)))
                    canvas.drawString(18 * cm, (hauteur - 0.5) * cm, str(round(equipe["total"], 2)))
                else:
                    canvas.drawString(17 * cm, hauteur * cm, str(equipe["total1"]))
                    canvas.drawString(17 * cm, (hauteur - 0.5) * cm, str(equipe["total2"]))
                    canvas.drawString(17 * cm, (hauteur - 1) * cm, str(equipe["total3"]))
                    canvas.drawString(18 * cm, (hauteur - 0.5) * cm, str(equipe["total"]))

                hauteur -= 0.5
                canvas.drawString(7 * cm, hauteur * cm, equipe["nom2"] + " " + equipe["prenom2"])
                canvas.drawString(12 * cm, hauteur * cm, seriesStr2)
                
                hauteur -= 0.5
                canvas.drawString(7 * cm, hauteur * cm, equipe["nom3"] + " " + equipe["prenom3"])
                canvas.drawString(12 * cm, hauteur * cm, seriesStr1)

                i += 1

                if hauteur <= 2 :
                    canvas.showPage()
                    hauteur = 28
                else: hauteur -= 1
    hauteur -= 1
    
    if hauteur <= 6 :
        canvas.showPage()
        hauteur = 28

    canvas.setFont("Helvetica-Bold", 14)
    canvas.drawString(2 * cm, hauteur * cm, "Palmares Super Challenge")
    hauteur -= 1

    challengeEquipesTriees = sorted(challengeEquipes, key=itemgetter('total'), reverse=True)

    i = 1
    for equipe in challengeEquipesTriees:
        seriesStr1 = ""
        seriesStr2 = ""
        seriesStr3 = ""
        seriesStr4 = ""
        seriesStr5 = ""
        for val in equipe["300score"]:
            seriesStr1 += str(val) + " "
        for val in equipe["400score1"]:
            seriesStr2 += str(val) + " "
        for val in equipe["400score2"]:
            seriesStr3 += str(val) + " "
        for val in equipe["600score1"]:
            seriesStr4 += str(val) + " "
        for val in equipe["600score2"]:
            seriesStr5 += str(val) + " "

        canvas.setFont("Helvetica-Bold", 12)
        canvas.drawString(8.7 * cm, hauteur * cm, str(i) + ". ")
        canvas.drawString(9.5 * cm, hauteur * cm, equipe["club"])

        hauteur -= 1

        canvas.setFont("Helvetica", 8)
        canvas.drawString(3 * cm, hauteur * cm, equipe["300nom"] + " " + equipe["300prenom"])
        canvas.drawString(12 * cm, hauteur * cm, seriesStr1)
        canvas.drawString(17 * cm, hauteur * cm, str(equipe["300total"]))

        hauteur -= 0.5
        canvas.drawString(3 * cm, hauteur * cm, equipe["400nom1"] + " " + equipe["400prenom1"])
        canvas.drawString(12 * cm, hauteur * cm, seriesStr2)
        canvas.drawString(17 * cm, hauteur * cm, str(equipe["400total1"]))
        
        hauteur -= 0.5
        canvas.drawString(3 * cm, hauteur * cm, equipe["400nom2"] + " " + equipe["400prenom2"])
        canvas.drawString(12 * cm, hauteur * cm, seriesStr3)
        canvas.drawString(17 * cm, hauteur * cm, str(equipe["400total2"]))
        canvas.drawString(18 * cm, hauteur * cm, str(equipe["total"]))

        hauteur -= 0.5
        canvas.drawString(3 * cm, hauteur * cm, equipe["600nom1"] + " " + equipe["600prenom1"])
        canvas.drawString(12 * cm, hauteur * cm, seriesStr4)
        canvas.drawString(17 * cm, hauteur * cm, str(equipe["600total1"]))

        hauteur -= 0.5
        canvas.drawString(3 * cm, hauteur * cm, equipe["600nom2"] + " " + equipe["600prenom2"])
        canvas.drawString(12 * cm, hauteur * cm, seriesStr5)
        canvas.drawString(17 * cm, hauteur * cm, str(equipe["600total2"]))

        i += 1

        if hauteur <= 2 :
            canvas.showPage()
            hauteur = 28
        else: hauteur -= 1

    if hauteur <= 4:
        canvas.showPage()
        hauteur = 28
    else: hauteur -= 1

    canvas.setFont("Helvetica-Bold", 12)
    canvas.drawString(8 * cm, hauteur * cm, "Nombre Participants : " + str(len(participants)))
    canvas.drawString(8.8 * cm, (hauteur - 1) * cm, "Nombre Clubs : " + str(len(clubs)))

    canvas.save()

def creer_equipe():
    global participants
    global clubs
    global equipes
    global categoriesList

    equipes = []

    for participant in participants:
        if (participant["club"] not in clubs): clubs.append(participant["club"])
    
    for discipline in disciplinesList:
        for club in clubs:
            for categorie in categoriesList:
                if (((discipline == 103) and (categorie == "D2")) or ((discipline == 102) and (categorie == "D2")) or ((discipline == 250) and (categorie == "D2")) or ((discipline == 252) and (categorie == "D2")) or ((discipline == 501) and (categorie == "D2"))):
                    categories = ["D2", "D3"]
                elif ((discipline == 103) and (categorie == "CG")):
                    categories = ["CG", "CF", "MG", "MF"]
                elif (((discipline == 252) and (categorie == "CG")) or ((discipline == 253) and (categorie == "CG"))):
                    categories = ["CG", "CF"]
                elif (((discipline == 252) and (categorie == "JG")) or ((discipline == 253) and (categorie == "JG"))):
                    categories = ["JG", "JF"]
                elif ((discipline == 253) and (categorie == "S1")):
                    categories = ["S1", "D1"]
                elif ((discipline == 253) and (categorie == "S2")):
                    categories = ["S2", "S3", "D2", "D3"]
                elif (((discipline == 831) and (categorie == "S1")) or ((discipline == 996) and (categorie == "S1"))):
                    categories = ["S1", "S2", "S3", "D1", "D2", "D3"]
                else:
                    categories = [categorie]

                equipe = {"discipline": discipline, "club": club, "categorie": categorie, "nom1": "", "prenom1": "", "total1": 0, "nom2": "", "prenom2": "", "total2": 0, "nom3": "", "prenom3": "", "total3": 0}
                for participant in participants:
                    try:
                        if ((participant["club"] == club) and (participant["categorie"] in categories) and (discipline in participant["disciplines"])):
                            if (equipe["nom1"] == ""):
                                equipe["nom1"] = participant["nom"]
                                equipe["prenom1"] = participant["prenom"]
                                equipe["score1"] = participant[str(discipline)]
                                equipe["total1"] = participant[str(discipline) + "total"]
                            elif (equipe["nom1"] != "" and equipe["nom2"] == ""):
                                if (equipe["total1"] < participant[str(discipline) + "total"]):
                                    equipe["nom2"] = equipe["nom1"]
                                    equipe["prenom2"] = equipe["prenom1"]
                                    equipe["score2"] = participant[str(discipline)]
                                    equipe["total2"] = equipe["total1"]

                                    equipe["nom1"] = participant["nom"]
                                    equipe["prenom1"] = participant["prenom"]
                                    equipe["score1"] = participant[str(discipline)]
                                    equipe["total1"] = participant[str(discipline) + "total"]
                                else:
                                    equipe["nom2"] = participant["nom"]
                                    equipe["prenom2"] = participant["prenom"]
                                    equipe["score2"] = participant[str(discipline)]
                                    equipe["total2"] = participant[str(discipline) + "total"]
                            elif (equipe["nom1"] != "" and equipe["nom2"] != "" and equipe["nom3"] == ""):
                                if (equipe["total2"] < participant[str(discipline) + "total"]):
                                    if (equipe["total1"] < participant[str(discipline) + "total"]):
                                        equipe["nom3"] = equipe["nom2"]
                                        equipe["prenom3"] = equipe["prenom2"]
                                        equipe["score3"] = equipe["score2"]
                                        equipe["total3"] = equipe["total2"]

                                        equipe["nom2"] = equipe["nom1"]
                                        equipe["prenom2"] = equipe["prenom1"]
                                        equipe["score2"] = equipe["score1"]
                                        equipe["total2"] = equipe["total1"]

                                        equipe["nom1"] = participant["nom"]
                                        equipe["prenom1"] = participant["prenom"]
                                        equipe["score1"] = participant[str(discipline)]
                                        equipe["total1"] = participant[str(discipline) + "total"]
                                    else:
                                        equipe["nom3"] = equipe["nom2"]
                                        equipe["prenom3"] = equipe["prenom2"]
                                        equipe["score3"] = equipe["score2"]
                                        equipe["total3"] = equipe["total2"]

                                        equipe["nom2"] = participant["nom"]
                                        equipe["prenom2"] = participant["prenom"]
                                        equipe["score2"] = participant[str(discipline)]
                                        equipe["total2"] = participant[str(discipline) + "total"]
                                else:
                                    equipe["nom3"] = participant["nom"]
                                    equipe["prenom3"] = participant["prenom"]
                                    equipe["score3"] = participant[str(discipline)]
                                    equipe["total3"] = participant[str(discipline) + "total"]
                                    
                            elif (equipe["nom1"] != "" and equipe["nom2"] != "" and equipe["nom3"] != ""):
                                if (participant[str(discipline) + "total"] > equipe["total3"]):
                                    if (participant[str(discipline) + "total"] > equipe["total2"]):
                                        if (participant[str(discipline) + "total"] > equipe["total1"]):
                                            equipe["nom3"] = equipe["nom2"]
                                            equipe["prenom3"] = equipe["prenom2"]
                                            equipe["score3"] = equipe["score2"]
                                            equipe["total3"] = equipe["total2"]

                                            equipe["nom2"] = equipe["nom1"]
                                            equipe["prenom2"] = equipe["prenom1"]
                                            equipe["score2"] = equipe["score1"]
                                            equipe["total2"] = equipe["total1"]

                                            equipe["nom1"] = participant["nom"]
                                            equipe["prenom1"] = participant["prenom"]
                                            equipe["score1"] = participant[str(discipline)]
                                            equipe["total1"] = participant[str(discipline) + "total"]
                                        else:
                                            equipe["nom3"] = equipe["nom2"]
                                            equipe["prenom3"] = equipe["prenom2"]
                                            equipe["score3"] = equipe["score2"]
                                            equipe["total3"] = equipe["total2"]

                                            equipe["nom2"] = participant["nom"]
                                            equipe["prenom2"] = participant["prenom"]
                                            equipe["score2"] = participant[str(discipline)]
                                            equipe["total2"] = participant[str(discipline) + "total"]
                                    else:
                                        equipe["nom3"] = participant["nom"]
                                        equipe["prenom3"] = participant["prenom"]
                                        equipe["score3"] = participant[str(discipline)]
                                        equipe["total3"] = participant[str(discipline) + "total"]
                    except: pass
                if (equipe["nom1"] != "" and equipe["nom2"] != "" and equipe["nom3"] != ""):
                    equipe["total"] = 0
                    for i in range(1, 4):
                        equipe["total"] += equipe["total" + str(i)]
                    equipes.append(equipe)

def creer_equipes_challenge():
    global participants
    global clubs
    global challengeEquipes
    global categoriesList
    global disciplinesList

    challengeEquipes = []

    for participant in participants:
        if (participant["club"] not in clubs): clubs.append(participant["club"])

    for club in clubs:
        equipe = {"club" : club, "300nom" : "", "300prenom" : "", "300total" : 0, "300discipline" : 0, "400nom1" : "", "400prenom1" : "", "400total1" : 0, "400discipline1": 0, "400nom2" : "", "400prenom2" : "", "400total2" : 0, "400disciplin2" : 0, "600nom1" : "", "600prenom1" : "", "600total1" : 0, "600discipline1" : 0, "600nom2" : "", "600prenom2" : "", "600total2" : 0, "600discipline2" : 0}
        for participant in participants:
            if (participant["club"] == club):
                try:
                    for discipline in [100, 104]:
                        if ((discipline in participant["disciplines"]) and (participant["categorie"] in ["PG", "PF", "BG", "BF"])):
                            if ((equipe["300total"] == 0) or (equipe["300total"] != 0) and (participant[str(discipline) + "total"] > equipe["300total"])):
                                equipe["300nom"] = participant["nom"]
                                equipe["300prenom"] = participant["prenom"]
                                equipe["300discipline"] = discipline
                                if (discipline == 100):
                                    equipe["300score"] = participant["100"]
                                    equipe["300total"] = participant["100total"]
                                elif (discipline == 104):
                                    equipe["300score"] = participant["104PE"]
                                    equipe["300total"] = participant["104PEtotal"]
                        elif ((discipline in participant["disciplines"]) and (participant["categorie"] in ["MG", "MF", "D3"])):
                            if (equipe["400total1"] == 0):
                                equipe["400nom1"] = participant["nom"]
                                equipe["400prenom1"] = participant["prenom"]
                                equipe["400discipline1"] = discipline
                                if (discipline == 100):
                                    equipe["400score1"] = participant["100"]
                                    equipe["400total1"] = participant["100total"]
                                elif (discipline == 104):
                                    equipe["400score1"] = participant["104PE"]
                                    equipe["400total1"] = participant["104PEtotal"]
                            elif ((equipe["400total1"] != 0) and (equipe["400total2"] == 0)):
                                if (participant[str(discipline) + "total"] < equipe["400total1"]):
                                    equipe["400nom2"] = participant["nom"]
                                    equipe["400prenom2"] = participant["prenom"]
                                    equipe["400discipline2"] = discipline
                                    if (discipline == 100):
                                        equipe["400score2"] = participant["100"]
                                        equipe["400total2"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["400score2"] = participant["104PE"]
                                        equipe["400total2"] = participant["104PEtotal"]
                                elif (participant[str(discipline) + "total"] > equipe["400total1"]):
                                    equipe["400nom2"] = equipe["400nom1"]
                                    equipe["400prenom2"] = equipe["400prenom1"]
                                    equipe["400score2"] = equipe["400score1"]
                                    equipe["400total2"] = equipe["400total1"]
                                    equipe["400discipline2"] = equipe["400discipline1"]

                                    equipe["400nom1"] = participant["nom"]
                                    equipe["400prenom1"] = participant["prenom"]
                                    equipe["400discipline1"] = discipline
                                    if (discipline == 100):
                                        equipe["400score1"] = participant["100"]
                                        equipe["400total1"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["400score1"] = participant["104PE"]
                                        equipe["400total1"] = participant["104PEtotal"]
                            elif ((equipe["400total1"] != 0) and (equipe["400total2"] != 0)):
                                if (participant[str(discipline) + "total"] > equipe["400total1"]):
                                    equipe["400nom2"] = equipe["400nom1"]
                                    equipe["400prenom2"] = equipe["400prenom1"]
                                    equipe["400score2"] = equipe["400score1"]
                                    equipe["400total2"] = equipe["400total1"]
                                    equipe["400discipline2"] = equipe["400discipline1"]

                                    equipe["400nom1"] = participant["nom"]
                                    equipe["400prenom1"] = participant["prenom"]
                                    equipe["400discipline1"] = discipline
                                    if (discipline == 100):
                                        equipe["400score1"] = participant["100"]
                                        equipe["400total1"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["400score1"] = participant["104PE"]
                                        equipe["400total1"] = participant["104PEtotal"]
                                elif ((participant[str(discipline) + "total"] < equipe["400total1"]) and (participant[str(discipline) + "total"] > equipe["400total2"])):
                                    equipe["400nom2"] = participant["nom"]
                                    equipe["400prenom2"] = participant["prenom"]
                                    equipe["400discipline2"] = discipline
                                    if (discipline == 100):
                                        equipe["400score2"] = participant["100"]
                                        equipe["400total2"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["400score2"] = participant["104PE"]
                                        equipe["400total2"] = participant["104PEtotal"]
                        elif ((discipline in participant["disciplines"]) and (participant["categorie"] in ["CG", "CF", "JG", "JF", "S1", "S2", "S3", "D1", "D2"])):
                            if (equipe["600total1"] == 0):
                                equipe["600nom1"] = participant["nom"]
                                equipe["600prenom1"] = participant["prenom"]
                                equipe["600discipline1"] = discipline
                                if (discipline == 100):
                                    equipe["600score1"] = participant["100"]
                                    equipe["600total1"] = participant["100total"]
                                elif (discipline == 104):
                                    equipe["600score1"] = participant["104PE"]
                                    equipe["600total1"] = participant["104PEtotal"]
                            elif ((equipe["600total1"] != 0) and (equipe["600total2"] == 0)):
                                if (participant[str(discipline) + "total"] < equipe["600total1"]):
                                    equipe["600nom2"] = participant["nom"]
                                    equipe["600prenom2"] = participant["prenom"]
                                    equipe["600discipline2"] = discipline
                                    if (discipline == 100):
                                        equipe["600score2"] = participant["100"]
                                        equipe["600total2"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["600score2"] = participant["104PE"]
                                        equipe["600total2"] = participant["104PEtotal"]
                                elif (participant[str(discipline) + "total"] > equipe["600total1"]):
                                    equipe["600nom2"] = equipe["600nom1"]
                                    equipe["600prenom2"] = equipe["600prenom1"]
                                    equipe["600score2"] = equipe["600score1"]
                                    equipe["600total2"] = equipe["600total1"]
                                    equipe["600discipline2"] = equipe["600discipline1"]

                                    equipe["600nom1"] = participant["nom"]
                                    equipe["600prenom1"] = participant["prenom"]
                                    equipe["600discipline1"] = discipline
                                    if (discipline == 100):
                                        equipe["600score1"] = participant["100"]
                                        equipe["600total1"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["600score1"] = participant["104PE"]
                                        equipe["600total1"] = participant["104PEtotal"]
                            elif ((equipe["600total1"] != 0) and (equipe["600total2"] != 0)):
                                if (participant[str(discipline) + "total"] > equipe["600total1"]):
                                    equipe["600nom2"] = equipe["600nom1"]
                                    equipe["600prenom2"] = equipe["600prenom1"]
                                    equipe["600score2"] = equipe["600score1"]
                                    equipe["600total2"] = equipe["600total1"]
                                    equipe["600discipline2"] = equipe["600discipline1"]

                                    equipe["600nom1"] = participant["nom"]
                                    equipe["600prenom1"] = participant["prenom"]
                                    equipe["600discipline1"] = discipline
                                    if (discipline == 100):
                                        equipe["600score1"] = participant["100"]
                                        equipe["600total1"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["600score1"] = participant["104PE"]
                                        equipe["600total1"] = participant["104PEtotal"]
                                elif ((participant[str(discipline) + "total"] < equipe["600total1"]) and (participant[str(discipline) + "total"] > equipe["600total2"])):
                                    equipe["600nom2"] = participant["nom"]
                                    equipe["600prenom2"] = participant["prenom"]
                                    equipe["600discipline2"] = discipline
                                    if (discipline == 100):
                                        equipe["600score2"] = participant["100"]
                                        equipe["600total2"] = participant["100total"]
                                    elif (discipline == 104):
                                        equipe["600score2"] = participant["104PE"]
                                        equipe["600total2"] = participant["104PEtotal"]
                    for discipline in [103, 183]:
                        if ((discipline in participant["disciplines"]) and (participant["categorie"] in ["CG", "CF", "JG", "JF", "S1", "S2", "S3", "D1", "D2", "D3", "PARA"])):
                                if (equipe["400total1"] == 0):
                                    equipe["400nom1"] = participant["nom"]
                                    equipe["400prenom1"] = participant["prenom"]
                                    equipe["400score1"] = participant[str(discipline)]
                                    equipe["400total1"] = participant[str(discipline) + "total"]
                                    equipe["400discipline1"] = discipline
                                elif ((equipe["400total1"] != 0) and (equipe["400total2"] == 0)):
                                    if (participant[str(discipline) + "total"] < equipe["400total1"]):
                                        equipe["400nom2"] = participant["nom"]
                                        equipe["400prenom2"] = participant["prenom"]
                                        equipe["400score2"] = participant[str(discipline)]
                                        equipe["400total2"] = participant[str(discipline) + "total"]
                                        equipe["400discipline2"] = discipline
                                    elif (participant[str(discipline) + "total"] > equipe["400total1"]):
                                        equipe["400nom2"] = equipe["400nom1"]
                                        equipe["400prenom2"] = equipe["400prenom1"]
                                        equipe["400score2"] = equipe["400score1"]
                                        equipe["400total2"] = equipe["400total1"]
                                        equipe["400discipline2"] = discipline

                                        equipe["400nom1"] = participant["nom"]
                                        equipe["400prenom1"] = participant["prenom"]
                                        equipe["400score1"] = participant[str(discipline)]
                                        equipe["400total1"] = participant[str(discipline) + "total"]
                                        equipe["400discipline1"] = discipline
                                elif ((equipe["400total1"] != 0) and (equipe["400total2"] != 0)):
                                    if (participant["103total"] > equipe["400total1"]):
                                        equipe["400nom2"] = equipe["400nom1"]
                                        equipe["400prenom2"] = equipe["400prenom1"]
                                        equipe["400score2"] = equipe["400score1"]
                                        equipe["400total2"] = equipe["400total1"]
                                        equipe["400discipline2"] = discipline

                                        equipe["400nom1"] = participant["nom"]
                                        equipe["400prenom1"] = participant["prenom"]
                                        equipe["400score1"] = participant[str(discipline)]
                                        equipe["400total1"] = participant[str(discipline) + "total"]
                                        equipe["400discipline1"] = discipline
                                    elif ((participant[str(discipline) + "total"] < equipe["400total1"]) and (participant[str(discipline) + "total"] > equipe["400total2"])):
                                        equipe["400nom2"] = participant["nom"]
                                        equipe["400prenom2"] = participant["prenom"]
                                        equipe["400score2"] = participant[str(discipline)]
                                        equipe["400total2"] = participant[str(discipline) + "total"]
                                        equipe["400discipline2"] = discipline
                    for discipline in [250, 252, 253, 501, 171, 172, 174, 181, 182, 183, 184, 271, 273, 274, 571, 572]:
                        if (discipline in participant["disciplines"]):
                            if (equipe["600total1"] == 0):
                                equipe["600nom1"] = participant["nom"]
                                equipe["600prenom1"] = participant["prenom"]
                                equipe["600discipline1"] = discipline
                                if (discipline in [250, 252, 253]):
                                    equipe["600score1"] = participant[str(discipline)]
                                    equipe["600total1"] = participant[str(discipline) + "total"]
                                elif (discipline in [501, 171, 172, 174, 571, 574]):
                                    equipe["600score1"] = participant[str(discipline) + "PE"]
                                    equipe["600total1"] = participant[str(discipline) + "PEtotal"]
                            elif ((equipe["600total1"] != 0) and (equipe["600total2"] == 0)):
                                if (participant[str(discipline) + "total"] < equipe["600total1"]):
                                    equipe["600nom2"] = participant["nom"]
                                    equipe["600prenom2"] = participant["prenom"]
                                    equipe["600discipline2"] = discipline
                                    if (discipline in [250, 252, 253]):
                                        equipe["600score2"] = participant[str(discipline)]
                                        equipe["600total2"] = participant[str(discipline) + "total"]
                                    elif (discipline in [501, 171, 172, 174, 571, 574]):
                                        equipe["600score2"] = participant[str(discipline) + "PE"]
                                        equipe["600total2"] = participant[str(discipline) + "PEtotal"]
                                elif (participant[str(discipline) + "total"] > equipe["600total1"]):
                                    equipe["600nom2"] = equipe["600nom1"]
                                    equipe["600prenom2"] = equipe["600prenom1"]
                                    equipe["600score2"] = equipe["600score1"]
                                    equipe["600total2"] = equipe["600total1"]
                                    equipe["600discipline2"] = equipe["600discipline1"]

                                    equipe["600nom1"] = participant["nom"]
                                    equipe["600prenom1"] = participant["prenom"]
                                    equipe["600discipline1"] = discipline
                                    if (discipline in [250, 252, 253]):
                                        equipe["600score1"] = participant[str(discipline)]
                                        equipe["600total1"] = participant[str(discipline) + "total"]
                                    elif (discipline in [501, 171, 172, 174, 571, 574]):
                                        equipe["600score1"] = [str(discipline) + "PE"]
                                        equipe["600total1"] = participant[str(discipline) + "PEtotal"]
                            elif ((equipe["600total1"] != 0) and (equipe["600total2"] != 0)):
                                if (participant[str(discipline) + "total"] > equipe["600total1"]):
                                    equipe["600nom2"] = equipe["600nom1"]
                                    equipe["600prenom2"] = equipe["600prenom1"]
                                    equipe["600score2"] = equipe["600score1"]
                                    equipe["600total2"] = equipe["600total1"]
                                    equipe["600discipline2"] = equipe["600discipline1"]

                                    equipe["600nom1"] = participant["nom"]
                                    equipe["600prenom1"] = participant["prenom"]
                                    equipe["600discipline1"] = discipline
                                    if (discipline in [250, 252, 253]):
                                        equipe["600score1"] = participant[str(discipline)]
                                        equipe["600total1"] = participant[str(discipline) + "total"]
                                    elif (discipline in [501, 171, 172, 174, 571, 574]):
                                        equipe["600score1"] = participant[str(discipline) + "PE"]
                                        equipe["600total1"] = participant[str(discipline) + "PEtotal"]
                                elif ((participant[str(discipline) + "total"] < equipe["600total1"]) and (participant[str(discipline) + "total"] > equipe["600total2"])):
                                    equipe["600nom2"] = participant["nom"]
                                    equipe["600prenom2"] = participant["prenom"]
                                    equipe["600discipline2"] = discipline
                                    if (discipline in [250, 252, 253]):
                                        equipe["600score2"] = participant[str(discipline)]
                                        equipe["600total2"] = participant[str(discipline) + "total"]
                                    elif (discipline in [501, 171, 172, 174, 571, 574]):
                                        equipe["600score2"] = participant[str(discipline) + "PE"]
                                        equipe["600total2"] = participant[str(discipline) + "PEtotal"]
                except: pass
        if ((equipe["300nom"] != "") and (equipe["400nom1"] != "") and (equipe["400nom2"] != "") and (equipe["600nom1"] != "") and (equipe["600nom2"] != "")):
            equipe["total"] = equipe["300total"] + equipe["400total1"] + equipe["400total2"] + equipe["600total1"] + equipe["600total2"]
            challengeEquipes.append(equipe)
            print("\n")


def save_json():
    global participants

    with open("save_participants.json", "w") as final:
        json.dump(participants, final)

def load_json():
    global participants

    if (os.path.exists("save_participants.json")):
        participants = []

        participantsFile = open("save_participants.json")
        participantsString = participantsFile.read()
        participants = json.loads(participantsString)
    else:
        with open("save_participants.json", "w") as nouveau:
            json.dump([], nouveau)

def to_string():
    global participants

    for participant in participants:
        toStr = participant["nom"] + " " + participant["prenom"] + " " + participant["club"] + " " + participant["categorie"] + " Disciplines :"
        for discipline in participant["disciplines"]:
            toStr += " " + str(discipline)
        print(toStr + "\n")

def quitter():
    save_json()
    fenetre.quit()

def fenetre_principale():
    
    titre.place(x=350, y=-130)
    boutonAjouter.place(x=325, y=75)
    infoAjouter.place(x=475, y=75)
    boutonDiscipline.place(x=325, y=150)
    infoDiscipline.place(x=475, y=150)
    boutonSupprimer.place(x=325, y=225)
    boutonSerie.place(x=325, y=300)
    boutonPDF.place(x=325, y=375)
    infoPDF.place(x=475, y=375)
    boutonSauvegarder.place(x=325, y=450)

quit = False
disciplinesList = [100, 102, 103, 104, 250, 251, 252, 253, 501, 831, 996, 171, 172, 174, 181, 182, 183, 184, 271, 273, 274, 571, 574]
categoriesList = ["PF", "PG", "BF", "BG", "MF", "MG", "CF", "CG", "JF", "JG", "D1", "D2", "D3", "S1", "S2", "S3", "SH1", "SH2", "PB", "PM", "PM21", "PP21"]
clubList = [ "CTS Cholet", "ATS Angers", "ASC Monfortaise", "STH Les Herbiers", "AST Tiercé", "SSTAR Saumur", "ETSL St Laurent", "ASTD Durtal", "CT Carquefou", "ETLS Les Sorinières", "RTS Rezé", "SNT Nantes", "ANT St Nazaire", "ASCE La Baule", "CTL La Limouzinière", "SBT Blain", "ST Pornic", "SPT Pontchâteau", "STMT St Michel", "TS Le Pellerin", "AT2000 Cholet", "CPSAD Angers", "RCD Puy Notre Dame", "BTCM St Loup", "CTMB Juvigné", "CTM Mayenne", "CGTS Château Gontier", "STL Laval", "ASTMS Mamers", "CTSM Ecommoy", "CSATA Champagné", "CS Sablé", "STAR Aubigné", "STM Le Mans", "STB Beaumont", "USSM St Mars", "VSF Ferté Bernard", "ASTIC Les Sables", "STLN Noirmoutier", "SMT St Jean", "STF Fontenay", "STPRV St Hilaire", "STVM Venansault"]

participants = []
clubs = []
equipes = []
challengeEquipes = []

requete = ""
load_json()

fenetre = Tk()
fenetre.geometry("800x600")

titre = Label(fenetre, text="Palmares Tir Sportif", height=20)
boutonAjouter = Button(fenetre, text="Ajouter Participant", height=3, width=20, command=ajouter_participant)
infoAjouter = Label(fenetre, text=": Seulement participants non-inscrits", height=3, width=30)
boutonDiscipline = Button(fenetre, text="Ajouter Disciplines", height=3, width=20, command=ajouter_discipline)
infoDiscipline = Label(fenetre, text=": Seulement participants déjà inscrits", height=3, width=30)
boutonSupprimer = Button(fenetre, text="Supprimer Participant", height=3, width=20, command=retirer_participant)
boutonSerie = Button(fenetre, text="Ajouter Séries", height=3, width=20, command=ajouter_series)
boutonPDF = Button(fenetre, text="Générer le Classement", height=3, width=20, command=creer_pdf)
infoPDF = Label(fenetre, text=": Bien fermer le PDF avant d'en générer un nouveau", height=3, width=40)
boutonSauvegarder = Button(fenetre, text="Quitter", height=3, width=20, command=quitter)

fenetre_principale()

fenetre.mainloop()